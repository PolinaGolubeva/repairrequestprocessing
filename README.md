#  The repair request processing application
Application is aimed to read data about repair requests from provided xlsx-file and return
required requests by year and municipality code. There are two options available: 
* fetch data as JSON-list (using endpoint `/api/year/{year}/municipality/{municipalityCode}`) 
* fetch data as xlsx-file
(using endpoint `/api/xlsx/year/{year}/municipality/{municipalityCode}`).

#### Configuration
Before application launch, please, copy file `resources/application.properties.example`
to the `application.properties` file at the same directory and change following properties
according to your settings:
* url of your database (`spring.datasource.url`)
* username of database user (`spring.datasource.username`)
* password (`spring.datasource.password`)
* schema if needed (`spring.flyway.schemas`)
* name and path to xlsx-file with data (`import.filename`)

If you want to import data at the start of the application, set the property `import.enable`
to `true`. The import is disabled by default.

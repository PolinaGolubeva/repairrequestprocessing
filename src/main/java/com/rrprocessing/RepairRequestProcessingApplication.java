package com.rrprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RepairRequestProcessingApplication {

    public static void main(String[] args) {
        SpringApplication.run(RepairRequestProcessingApplication.class, args);
    }

}

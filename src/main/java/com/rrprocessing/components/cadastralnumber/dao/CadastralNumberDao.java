package com.rrprocessing.components.cadastralnumber.dao;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import java.util.List;
import java.util.Optional;

/**
 * DAO working with <code>CadastralNumber</code>
 */
public interface CadastralNumberDao {

    /**
     * Finds a cadastral number record in database by its string value if it exists
     *
     * @param cadNumber value to be found in database
     * @return <code>Optional</code> of cadastral number
     */
    Optional<CadastralNumber> find(String cadNumber);

    /**
     * Finds a list of cadastral number records in database by its string values
     *
     * @param cadNumbers values to be found in the database
     * @return <code>List</code> of found cadastral numbers
     */
    List<CadastralNumber> find(List<String> cadNumbers);

    /**
     * Saves cadastral number to the database
     *
     * @param cadastralNumber value to be saved
     * @return id of saved cadastral number record
     */
    Long save(CadastralNumber cadastralNumber);

    /**
     * Saves multiple cadastral numbers to the database
     *
     * @param cadastralNumbers list of values to be saved
     */
    void save(List<CadastralNumber> cadastralNumbers);
}

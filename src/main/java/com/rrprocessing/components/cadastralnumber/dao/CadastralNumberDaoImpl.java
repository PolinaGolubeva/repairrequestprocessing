package com.rrprocessing.components.cadastralnumber.dao;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.components.cadastralnumber.mapper.CadastralNumberMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation of <code>CadastralNumberDao</code>, working with <code>NamedParameterJdbcTemplate</code>
 */
@Repository
@Slf4j
public class CadastralNumberDaoImpl implements CadastralNumberDao {

    private static final String SINGLE_SELECT_QUERY = "SELECT id AS cadastral_id, cad_number FROM cadastral_number " +
            "WHERE cad_number = :cadastralNumber";
    private static final String INSERT_QUERY = "INSERT INTO cadastral_number (cad_number) VALUES (:cadastralNumber) " +
            "ON CONFLICT DO NOTHING";
    private static final String SELECT_LIST_QUERY = "SELECT id AS cadastral_id, cad_number FROM cadastral_number " +
            "WHERE cad_number IN (:numbers)";

    private final NamedParameterJdbcTemplate template;

    /**
     * Creates <code>CadastralNumberDaoImpl</code> object with configured template
     *
     * @param dataSource
     */
    @Autowired
    public CadastralNumberDaoImpl(DataSource dataSource) {

        this.template = new NamedParameterJdbcTemplate(dataSource);
    }

    /**
     * Finds a cadastral number record in database by its string value if it exists
     *
     * @param cadNumber value to be found in database
     * @return <code>Optional</code> of cadastral number
     */
    @Override
    public Optional<CadastralNumber> find(String cadNumber) {

        CadastralNumber cadastralNumber = null;
        try {
            cadastralNumber = template.queryForObject(SINGLE_SELECT_QUERY, Collections.singletonMap("cadastralNumber", cadNumber),
                    new CadastralNumberMapper());
        } catch (DataAccessException e) {
            log.warn("Cadastral number {} is not found", cadNumber, e);
        }

        return Optional.ofNullable(cadastralNumber);
    }

    /**
     * Finds a list of cadastral number records in database by its string values
     *
     * @param cadNumbers values to be found in the database
     * @return <code>List</code> of found cadastral numbers
     */
    @Override
    public List<CadastralNumber> find(List<String> cadNumbers) {

        SqlParameterSource parameterListSource = getListParameterSource(cadNumbers);

        return template.queryForStream(SELECT_LIST_QUERY, parameterListSource, new CadastralNumberMapper())
                .collect(Collectors.toList());
    }

    /**
     * Saves cadastral number to the database
     *
     * @param cadastralNumber value to be saved
     * @return id of saved cadastral number record
     */
    @Override
    public Long save(CadastralNumber cadastralNumber) {

        SqlParameterSource parameterSource = getParameterSource(cadastralNumber);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(INSERT_QUERY, parameterSource, keyHolder, new String[]{"id"});

        if (keyHolder.getKey() == null) {
            log.warn(String.format("Cadastral number %s was not saved", cadastralNumber));

            return null;
        }

        return Objects.requireNonNull(keyHolder.getKey()).longValue();
    }

    /**
     * Saves multiple cadastral numbers to the database
     *
     * @param cadastralNumbers list of values to be saved
     */
    @Override
    public void save(List<CadastralNumber> cadastralNumbers) {

        SqlParameterSource[] parameterSources = getParameterSources(cadastralNumbers);
        template.batchUpdate(INSERT_QUERY, parameterSources);
    }

    private SqlParameterSource[] getParameterSources(List<CadastralNumber> cadastralNumbers) {

        SqlParameterSource[] parameterSources = new SqlParameterSource[cadastralNumbers.size()];
        for (int i = 0; i < parameterSources.length; i++) {
            parameterSources[i] = getParameterSource(cadastralNumbers.get(i));
        }

        return parameterSources;
    }

    private SqlParameterSource getParameterSource(CadastralNumber cadastralNumber) {

        return new MapSqlParameterSource(Collections.singletonMap("cadastralNumber",
                cadastralNumber.getNumber()));
    }

    private SqlParameterSource getListParameterSource(List<String> cadNumbers) {

        Map<String, Object> parameterMap = Collections.singletonMap("numbers", cadNumbers);

        return new MapSqlParameterSource(parameterMap);
    }
}

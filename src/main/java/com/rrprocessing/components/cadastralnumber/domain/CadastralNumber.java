package com.rrprocessing.components.cadastralnumber.domain;

import lombok.Data;

/**
 * Entity class representing cadastral number
 */
@Data
public class CadastralNumber {

    /**
     * Id of cadastral number record
     */
    private Long id;

    /**
     * Cadastral number value
     */
    private String number;
}

package com.rrprocessing.components.cadastralnumber.mapper;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper required to convert data from result set to <code>CadastralNumber</code> instance
 */
public class CadastralNumberMapper implements RowMapper<CadastralNumber> {

    /**
     * Maps <code>ResultSet</code> to <code>CadastralNumber</code>
     *
     * @param rs result set to be converted
     * @param rowNum index of current row in the result set
     * @return <code>CadastralNumber</code> instance
     * @throws SQLException
     */
    @Override
    public CadastralNumber mapRow(ResultSet rs, int rowNum) throws SQLException {

        CadastralNumber cadastralNumber = new CadastralNumber();
        cadastralNumber.setId(rs.getLong("cadastral_id"));
        cadastralNumber.setNumber(rs.getString("cad_number"));

        return cadastralNumber;
    }
}

package com.rrprocessing.components.cadastralnumber.service;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import java.util.List;
import java.util.Optional;

/**
 * The <code>CadastralNumberService</code> works with <code>CadastralNumber</code> instances.
 * Provides with methods to retrieve required instances and to save given instances.
 */
public interface CadastralNumberService {

    /**
     * Saves cadastral number to the database and returns saved instance
     *
     * @param cadastralNumber
     * @return saved <code>CadastralNumber</code> instance
     */
    CadastralNumber save(CadastralNumber cadastralNumber);

    /**
     * Saves list of cadastral numbers to the database and returns saved instances
     *
     * @param cadastralNumbers
     * @return <code>List</code> of saved instances
     */
    List<CadastralNumber> save(List<CadastralNumber> cadastralNumbers);

    /**
     * Finds cadastral number instance by its text value
     *
     * @param cadastralNumber
     * @return <code>Optional</code> of found cadastral number
     */
    Optional<CadastralNumber> find(String cadastralNumber);

    /**
     * Finds cadastral number instances by its text values
     *
     * @param cadastralNumbers
     * @return <code>List</code> of found cadatral numbers
     */
    List<CadastralNumber> find(List<String> cadastralNumbers);
}

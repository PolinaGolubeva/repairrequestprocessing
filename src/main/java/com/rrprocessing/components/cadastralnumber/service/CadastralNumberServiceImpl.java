package com.rrprocessing.components.cadastralnumber.service;

import com.rrprocessing.components.cadastralnumber.dao.CadastralNumberDao;
import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.exceptions.EntityNotSavedException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation of <code>CadastralNumberService</code>
 */
@Service
@AllArgsConstructor
@Slf4j
public class CadastralNumberServiceImpl implements CadastralNumberService {

    private CadastralNumberDao cadastralNumberDao;

    /**
     * Saves cadastral number to the database and returns saved instance
     *
     * @param cadastralNumber
     * @return saved <code>CadastralNumber</code> instance
     */
    @Override
    @Transactional
    public CadastralNumber save(CadastralNumber cadastralNumber) {

        Optional<CadastralNumber> cadastralNumberOptional = find(cadastralNumber.getNumber());
        if (cadastralNumberOptional.isPresent()) {
            log.info(String.format("Cadastral number %s already exists in the database", cadastralNumber.getNumber()));

            return cadastralNumberOptional.get();
        }

        Long id = cadastralNumberDao.save(cadastralNumber);
        if (id == null) {
            throw new EntityNotSavedException(String.format("Cadastral number %s was not saved in the database",
                    cadastralNumber.getNumber()));
        }
        cadastralNumber.setId(id);

        return cadastralNumber;
    }

    /**
     * Saves list of cadastral numbers to the database and returns saved instances
     *
     * @param cadastralNumbers
     * @return <code>List</code> of saved instances
     */
    @Override
    public List<CadastralNumber> save(List<CadastralNumber> cadastralNumbers) {

        if (cadastralNumbers.isEmpty()) {
            return Collections.emptyList();
        }

        List<CadastralNumber> distinctNumbers = cadastralNumbers.stream()
                .distinct()
                .collect(Collectors.toList());

        cadastralNumberDao.save(distinctNumbers);
        List<CadastralNumber> savedCadastralNumbers = find(getNumbersList(distinctNumbers)).stream()
                .filter(cadastralNumber -> cadastralNumber.getId() != null)
                .collect(Collectors.toList());
        if (savedCadastralNumbers.size() != distinctNumbers.size()) {
            throw new EntityNotSavedException("Cadastral numbers were not saved");
        }

        return savedCadastralNumbers;
    }

    /**
     * Finds cadastral number instance by its text value
     *
     * @param cadastralNumber
     * @return <code>Optional</code> of found cadastral number
     */
    @Override
    public Optional<CadastralNumber> find(String cadastralNumber) {

        return cadastralNumberDao.find(cadastralNumber);
    }

    /**
     * Finds cadastral number instances by its text values
     *
     * @param cadastralNumbers
     * @return <code>List</code> of found cadatral numbers
     */
    @Override
    public List<CadastralNumber> find(List<String> cadastralNumbers) {

        return cadastralNumberDao.find(cadastralNumbers);
    }

    private List<String> getNumbersList(List<CadastralNumber> cadastralNumbers) {

        return cadastralNumbers.stream()
                .map(CadastralNumber::getNumber)
                .collect(Collectors.toList());
    }
}

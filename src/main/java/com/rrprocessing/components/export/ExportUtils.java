package com.rrprocessing.components.export;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

/**
 * The <code>ExportUtils</code> class contains methods required to create <code>HttpEntity</code>
 * containing byte array of xlsx-file
 */
@Component
public class ExportUtils {

    private static final String FILE_NAME_PATTERN = "Requests_%s_year_%s_code.xlsx";

    /**
     * Creates HttpEntity containing byte array of xlsx-file
     *
     * @param content byte array of xlsx-file to be sent
     * @param year of requests to be sent in file
     * @param municipalityCode of requests to be sent in file
     * @return <code>HttpEntity</code> containing byte array resource of xlsx-file
     */
    public static HttpEntity<ByteArrayResource> downloadXlsx(byte[] content, Integer year, Integer municipalityCode) {

        String fileName = String.format(FILE_NAME_PATTERN, year, municipalityCode);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "force-download"));
        header.set("Content-Disposition", "attachment; filename=" + fileName);

        return new HttpEntity<>(new ByteArrayResource(content), header);
    }
}

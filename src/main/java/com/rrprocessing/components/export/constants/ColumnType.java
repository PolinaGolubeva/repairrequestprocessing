package com.rrprocessing.components.export.constants;

import lombok.Getter;

/**
 * Enum used for xls-table creation. Contains names and widths of columns included in the table.
 */
public enum ColumnType {

    INDEX("№ п/п", 1500),
    OBJECT_NAME("Наименование объекта недвижимого имущества, подлежащего ремонту", 8000),
    ADDRESS("Адрес месторасположения", 8000),
    CADASTRAL_NUMBER("Кадастровый номер", 6000),
    ANOTHER_NUMBER("Иной учетный номер (при наличии)", 6000),
    AREA("Площадь объекта (кв. м)", 4000),
    WORK_TYPE("Виды работ", 4000),
    PLANNED_COST("Сумма планируемых расходов", 6000);

    /**
     * Title of column
     */
    @Getter
    private final String name;

    /**
     * Width of column in units of 1/256th of a character width
     */
    @Getter
    private final int width;

    ColumnType(String name, int width) {

        this.name = name;
        this.width = width;
    }
}

package com.rrprocessing.components.export.constants;

import java.util.List;

/**
 * Utility class for <code>XlsCreator</code>, containing names of xlsx-file's sheet and columns.
 */
public class Constants {

    /**
     * Name of sheet in xlsx-table
     */
    public static final String SHEET_NAME = "Заявки";

    /**
     * List of columns included in the xlsx-table with fixed order
     */
    public static final List<ColumnType> COLUMNS = List.of(ColumnType.INDEX, ColumnType.OBJECT_NAME,
            ColumnType.ADDRESS, ColumnType.CADASTRAL_NUMBER, ColumnType.ANOTHER_NUMBER, ColumnType.AREA,
            ColumnType.WORK_TYPE, ColumnType.PLANNED_COST);
}

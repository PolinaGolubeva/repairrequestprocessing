package com.rrprocessing.components.export.controller;

import com.rrprocessing.components.export.ExportUtils;
import com.rrprocessing.components.export.service.XlsExportService;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The <code>XlsController</code> processes http-requests getting xlsx-files with required content
 */
@Controller
@AllArgsConstructor
@RequestMapping("/api/xlsx")
public class XlsController {

    private XlsExportService xlsExportService;

    /**
     * Finds repair requests by year and municipality code and returns <code>HttpEntity</code>
     * containing attached xlsx-file with acquired data
     *
     * @param year
     * @param municipalityCode
     * @return <code>HttpEntity</code> including byte array of xlsx-file
     */
    @GetMapping(value = "/year/{year}/municipality/{municipalityCode}")
    @ResponseBody
    public HttpEntity<ByteArrayResource> getByYearAndMunicipalityCode(@PathVariable Integer year,
                                                                      @PathVariable Integer municipalityCode) {

        byte[] content = xlsExportService.getRequestsExcel(year, municipalityCode);

        return ExportUtils.downloadXlsx(content, year, municipalityCode);
    }
}

package com.rrprocessing.components.export.creator;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.components.export.constants.ColumnType;
import com.rrprocessing.components.export.constants.Constants;
import com.rrprocessing.components.request.domain.RepairRequest;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.DoubleStream;

/**
 * The <code>XlsCreator</code> is used to create xlsx-file basing on provided list of repair requests
 */
@Component
public class XlsCreator {

    private XSSFWorkbook workbook;
    private Sheet sheet;
    private CellStyle headerCellStyle;
    private CellStyle defaultCellStyle;
    private CellStyle footerCellStyle;
    private Map<ColumnType, BiConsumer<Cell, Object>> cellFillMap;

    /**
     * Creates xls-file filled with data from provided repair requests
     *
     * @param requests
     * @return byte array of resulting xlsx-file
     */
    public byte[] getRequestXls(List<RepairRequest> requests) throws IOException {

        initCellFillMap();
        initWorkbook();
        initHeader();
        insertData(requests);
        insertFooter(requests);

        return generateXls(workbook);
    }

    private void initCellFillMap() {

        cellFillMap = new EnumMap<>(ColumnType.class);
        cellFillMap.put(ColumnType.INDEX, this::fillNumberCell);
        cellFillMap.put(ColumnType.OBJECT_NAME, this::fillObjectNameCell);
        cellFillMap.put(ColumnType.ADDRESS, this::fillAddressCell);
        cellFillMap.put(ColumnType.CADASTRAL_NUMBER, this::fillCadastralNumberCell);
        cellFillMap.put(ColumnType.ANOTHER_NUMBER, this::fillAnotherNumberCell);
        cellFillMap.put(ColumnType.AREA, this::fillAreaCell);
        cellFillMap.put(ColumnType.WORK_TYPE, this::fillWorkTypeCell);
        cellFillMap.put(ColumnType.PLANNED_COST, this::fillPlannedCostCell);
    }

    private void initWorkbook() {

        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet(Constants.SHEET_NAME);

        XSSFFont defaultFont = workbook.createFont();
        defaultFont.setFontHeightInPoints((short)10);
        defaultFont.setFontName("Arial");
        defaultFont.setColor(IndexedColors.BLACK.getIndex());
        defaultFont.setBold(false);

        XSSFFont headerFont = workbook.createFont();
        headerFont.setFontHeightInPoints((short)10);
        headerFont.setFontName("Arial");
        headerFont.setColor(IndexedColors.BLACK.getIndex());
        headerFont.setBold(true);

        defaultCellStyle = workbook.createCellStyle();
        defaultCellStyle.setFont(defaultFont);
        defaultCellStyle.setBorderBottom(BorderStyle.THIN);
        defaultCellStyle.setBorderTop(BorderStyle.THIN);
        defaultCellStyle.setBorderLeft(BorderStyle.THIN);
        defaultCellStyle.setBorderRight(BorderStyle.THIN);
        defaultCellStyle.setWrapText(true);

        headerCellStyle = workbook.createCellStyle();
        headerCellStyle.cloneStyleFrom(defaultCellStyle);
        headerCellStyle.setFont(headerFont);
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

        footerCellStyle = workbook.createCellStyle();
        footerCellStyle.setFont(headerFont);
        footerCellStyle.setAlignment(HorizontalAlignment.RIGHT);
        footerCellStyle.setBorderBottom(BorderStyle.THIN);
        footerCellStyle.setBorderTop(BorderStyle.THIN);
        footerCellStyle.setBorderLeft(BorderStyle.THIN);
        footerCellStyle.setBorderRight(BorderStyle.THIN);
    }

    private void initHeader() {

        Row titleRow = sheet.createRow(0);
        Row numRow = sheet.createRow(1);

        for (int i = 0; i < Constants.COLUMNS.size(); i++) {
            Cell cell = titleRow.createCell(i);
            cell.setCellStyle(headerCellStyle);
            cell.setCellValue(Constants.COLUMNS.get(i).getName());
            sheet.setColumnWidth(i, Constants.COLUMNS.get(i).getWidth());

            cell = numRow.createCell(i);
            cell.setCellStyle(headerCellStyle);
            cell.setCellValue(i + 1);
        }
    }

    private void insertData(List<RepairRequest> requests) {

        int requestNum = 0;
        int lastRowIndex = sheet.getLastRowNum();

        for (RepairRequest request : requests) {
            Row row = sheet.createRow(++lastRowIndex);
            requestNum++;
            int cellIndex = 0;

            for (ColumnType column : Constants.COLUMNS) {
                Cell cell = row.createCell(cellIndex++);
                cell.setCellStyle(defaultCellStyle);

                BiConsumer<Cell, Object> biConsumer = cellFillMap.get(column);
                if (column.equals(ColumnType.INDEX)) {
                    biConsumer.accept(cell, requestNum);
                } else {
                    biConsumer.accept(cell, request);
                }
            }
        }
    }

    private void insertFooter(List<RepairRequest> requests) {

        int lastRowIndex = sheet.getLastRowNum();
        int colNumber = Constants.COLUMNS.size();
        Row footer = sheet.createRow(++lastRowIndex);

        for (int i = 0; i < colNumber - 1; i++) {
            Cell cell = footer.createCell(i);
            cell.setCellStyle(footerCellStyle);
        }

        CellRangeAddress mergedCells = new CellRangeAddress(lastRowIndex, lastRowIndex, 0, colNumber - 2);
        sheet.addMergedRegion(mergedCells);
        Cell total = footer.getCell(0);
        total.setCellValue("Итого:");

        double sum = requests.stream()
                .flatMapToDouble(repairRequest -> DoubleStream.of(repairRequest.getDeclaredCost()))
                .sum();
        Cell sumCell = footer.createCell(colNumber - 1);
        sumCell.setCellStyle(footerCellStyle);
        sumCell.setCellValue(sum);
    }

    private byte[] generateXls(XSSFWorkbook workbook) throws IOException {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        workbook.write(outputStream);
        workbook.close();

        return outputStream.toByteArray();
    }

    void fillNumberCell(Cell cell, Object number) {

        Integer index = (Integer) number;
        cell.setCellValue(index);
    }

    void fillObjectNameCell(Cell cell, Object requestObject) {

        RepairRequest request = (RepairRequest) requestObject;
        cell.setCellValue(request.getObjectName());
    }

    void fillAddressCell(Cell cell, Object requestObject) {

        RepairRequest request = (RepairRequest) requestObject;
        String address = request.getAddress();
        cell.setCellValue(address);
    }

    void fillCadastralNumberCell(Cell cell, Object requestObject) {

        RepairRequest request = (RepairRequest) requestObject;
        List<CadastralNumber> cadastralNumbers = request.getCadastralNumbers();

        String separator = "\n";
        StringBuilder numberBuilder = new StringBuilder();
        for (CadastralNumber cadastralNumber : cadastralNumbers) {
            numberBuilder.append(cadastralNumber.getNumber()).append(separator);
        }

        int lastSeparatorIndex = numberBuilder.lastIndexOf(separator);
        if (lastSeparatorIndex >= 0) {
            numberBuilder.deleteCharAt(lastSeparatorIndex);
        }

        cell.setCellValue(numberBuilder.toString());
    }

    void fillAnotherNumberCell(Cell cell, Object requestObject) {

        RepairRequest request = (RepairRequest) requestObject;
        String additionalNumber = request.getAdditionalNumber();
        cell.setCellValue(additionalNumber);
    }

    void fillAreaCell(Cell cell, Object requestObject) {

        RepairRequest request = (RepairRequest) requestObject;
        Double area = request.getArea();
        cell.setCellValue(area);
    }

    void fillWorkTypeCell(Cell cell, Object requestObject) {

        RepairRequest request = (RepairRequest) requestObject;
        String workType = request.getWorkType();
        cell.setCellValue(workType);
    }

    void fillPlannedCostCell(Cell cell, Object requestObject) {

        RepairRequest request = (RepairRequest) requestObject;
        Double cost = request.getDeclaredCost();
        cell.setCellValue(cost);
    }
}

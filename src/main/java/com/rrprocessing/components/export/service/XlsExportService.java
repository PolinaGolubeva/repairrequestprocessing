package com.rrprocessing.components.export.service;

/**
 * The <code>XlsExportService</code> provides with methods to write repair request to xlsx-file
 */
public interface XlsExportService {

    /**
     * Gets repair requests by year and municipality code and transfers to xlsx-file's byte array
     *
     * @param year
     * @param municipalityCode
     * @return byte array of produced xlsx-file
     */
    byte[] getRequestsExcel(Integer year, Integer municipalityCode);
}

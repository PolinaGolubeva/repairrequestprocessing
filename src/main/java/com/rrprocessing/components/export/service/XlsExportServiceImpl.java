package com.rrprocessing.components.export.service;

import com.rrprocessing.components.request.domain.RepairRequest;
import com.rrprocessing.exceptions.XlsCreationException;
import com.rrprocessing.components.request.service.RequestService;
import com.rrprocessing.components.export.creator.XlsCreator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.List;

/**
 * Implementation of <code>XlsExportService</code>
 */
@Service
@AllArgsConstructor
public class XlsExportServiceImpl implements XlsExportService {

    private RequestService requestService;
    private XlsCreator xlsCreator;

    /**
     * Gets repair requests by year and municipality code and transfers to xlsx-file's byte array
     *
     * @param year
     * @param municipalityCode
     * @return byte array of produced xlsx-file
     */
    @Override
    public byte[] getRequestsExcel(Integer year, Integer municipalityCode) {

        List<RepairRequest> requests = requestService.find(year, municipalityCode);

        try {
            return xlsCreator.getRequestXls(requests);
        } catch (IOException e) {
            throw new XlsCreationException(e.getMessage());
        }
    }
}

package com.rrprocessing.components.municipality.dao;

import com.rrprocessing.components.municipality.domain.Municipality;
import java.util.List;
import java.util.Optional;

/**
 * DAO working with <code>Municipality</code>
 */
public interface MunicipalityDao {

    /**
     * Finds a municipality record in database by its code if it exists
     *
     * @param code of municipality to be found
     * @return <code>Optional</code> of municipality record
     */
    Optional<Municipality> find(Integer code);

    /**
     * Finds municipality records in database by its codes
     *
     * @param codes of municipalities to be found
     * @return <code>List</code> of found municipality records
     */
    List<Municipality> find(List<Integer> codes);

    /**
     * Saves municipality to the database
     *
     * @param municipality value to be inserted
     * @return id of saved municipality record
     */
    Long save(Municipality municipality);

    /**
     * Saves multiple municipalities to the database
     *
     * @param municipalities list of values to be inserted
     */
    void save(List<Municipality> municipalities);
}

package com.rrprocessing.components.municipality.dao;

import com.rrprocessing.components.municipality.domain.Municipality;
import com.rrprocessing.components.municipality.mapper.MunicipalityMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation of <code>MunicipalityDao</code> working with <code>NamedParameterJdbcTemplate</code>
 */
@Repository
@Slf4j
public class MunicipalityDaoImpl implements MunicipalityDao {

    private final NamedParameterJdbcTemplate template;

    private static final String SELECT_QUERY = "SELECT id AS municipality_id, code AS municipality_code, " +
            "title AS municipality_title FROM municipality WHERE code = :code";
    private static final String INSERT_QUERY = "INSERT INTO municipality (code, title) VALUES (:code, :title) " +
            "ON CONFLICT DO NOTHING";
    private static final String SELECT_LIST_QUERY = "SELECT id AS municipality_id, code AS municipality_code, " +
            "title AS municipality_title FROM municipality " +
            "WHERE code IN (:municipalityCodes)";

    /**
     * Creates <code>MunicipalityDaoImpl</code> object with configured template
     *
     * @param dataSource
     */
    @Autowired
    public MunicipalityDaoImpl(DataSource dataSource) {

        this.template = new NamedParameterJdbcTemplate(dataSource);
    }

    /**
     * Finds a municipality record in database by its code if it exists
     *
     * @param code of municipality to be found
     * @return <code>Optional</code> of municipality record
     */
    @Override
    public Optional<Municipality> find(Integer code) {

        Municipality municipality = null;
        try {
            municipality = template.queryForObject(SELECT_QUERY, Collections.singletonMap("code", code),
                    new MunicipalityMapper());
        } catch (DataAccessException e) {
            log.warn("Municipality with code {} was not found", code, e);
        }

        return Optional.ofNullable(municipality);
    }

    /**
     * Finds municipality records in database by its codes
     *
     * @param codes of municipalities to be found
     * @return <code>List</code> of found municipality records
     */
    @Override
    public List<Municipality> find(List<Integer> codes) {

        SqlParameterSource listParameterSource = getListParameterSource(codes);

        return template.queryForStream(SELECT_LIST_QUERY, listParameterSource, new MunicipalityMapper())
                .collect(Collectors.toList());
    }

    /**
     * Saves municipality to the database
     *
     * @param municipality value to be inserted
     * @return id of saved municipality record
     */
    @Override
    public Long save(Municipality municipality) {

        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource parameterSource = getParameterSource(municipality);

        template.update(INSERT_QUERY, parameterSource, keyHolder, new String[]{"id"});

        if (keyHolder.getKey() == null) {
            log.warn(String.format("Municipality with code %s was not saved", municipality.getCode()));

            return null;
        }

        return Objects.requireNonNull(keyHolder.getKey()).longValue();
    }

    /**
     * Saves multiple municipalities to the database
     *
     * @param municipalities list of values to be inserted
     */
    @Override
    public void save(List<Municipality> municipalities) {

        SqlParameterSource[] parameterSources = getParameterSources(municipalities);
        template.batchUpdate(INSERT_QUERY, parameterSources);
    }

    private SqlParameterSource getParameterSource(Municipality municipality) {

        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("code", municipality.getCode());
        parameterMap.put("title", municipality.getTitle());

        return new MapSqlParameterSource(parameterMap);
    }

    private SqlParameterSource[] getParameterSources(List<Municipality> municipalities) {

        SqlParameterSource[] parameterSources = new SqlParameterSource[municipalities.size()];
        for (int i = 0; i < parameterSources.length; i++) {
            parameterSources[i] = getParameterSource(municipalities.get(i));
        }

        return parameterSources;
    }

    private SqlParameterSource getListParameterSource(List<Integer> codes) {

        Map<String, Object> parameters = Collections.singletonMap("municipalityCodes", codes);

        return new MapSqlParameterSource(parameters);
    }
}

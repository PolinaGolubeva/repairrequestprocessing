package com.rrprocessing.components.municipality.domain;

import lombok.Data;

/**
 * Entity class representing municipality with specific code and title
 */
@Data
public class Municipality {

    /**
     * Id of municipality record
     */
    private Long id;

    /**
     * Unique municipality code
     */
    private Integer code;

    /**
     * Unique municipality title
     */
    private String title;
}

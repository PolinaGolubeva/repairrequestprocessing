package com.rrprocessing.components.municipality.mapper;

import com.rrprocessing.components.municipality.domain.Municipality;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper required to convert data from <code>ResultSet</code> row to <code>Municipality</code>
 */
public class MunicipalityMapper implements RowMapper<Municipality> {

    /**
     * Maps <code>ResultSet</code> row to <code>Municipality</code>
     *
     * @param rs result set to be converted
     * @param rowNum index of current row in the result set
     * @return <code>Municipality</code> instance
     * @throws SQLException
     */
    @Override
    public Municipality mapRow(ResultSet rs, int rowNum) throws SQLException {

        Municipality municipality = new Municipality();
        municipality.setId(rs.getLong("municipality_id"));
        municipality.setCode(rs.getInt("municipality_code"));
        municipality.setTitle(rs.getString("municipality_title"));

        return municipality;
    }
}

package com.rrprocessing.components.municipality.service;

import com.rrprocessing.components.municipality.domain.Municipality;
import java.util.List;
import java.util.Optional;

/**
 * The <code>MunicipalityService</code> works with <code>Municipality</code> instances.
 * Provides with methods to retrieve required instances and to save given instances.
 */
public interface MunicipalityService {

    /**
     * Saves given municipality in the database and returns saved instance
     *
     * @param municipality
     * @return saved <code>Municipality</code> instance
     */
    Municipality save(Municipality municipality);

    /**
     * Saves given list of municipalities and returns list of saved instances
     *
     * @param municipalities
     * @return <code>List</code> of saved <code>Municipality</code> instances
     */
    List<Municipality> save(List<Municipality> municipalities);

    /**
     * Finds municipality by its code
     *
     * @param municipalityCode
     * @return <code>Optional</code> of found municipality
     */
    Optional<Municipality> find(Integer municipalityCode);

    /**
     * Finds municipalities by its codes
     *
     * @param municipalityCodes
     * @return <code>List</code> of found municipalities
     */
    List<Municipality> find(List<Integer> municipalityCodes);
}

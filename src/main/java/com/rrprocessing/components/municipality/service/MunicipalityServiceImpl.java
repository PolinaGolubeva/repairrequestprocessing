package com.rrprocessing.components.municipality.service;

import com.rrprocessing.components.municipality.dao.MunicipalityDao;
import com.rrprocessing.components.municipality.domain.Municipality;
import com.rrprocessing.exceptions.EntityNotSavedException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation of <code>MunicipalityService</code>
 */
@Service
@AllArgsConstructor
@Slf4j
public class MunicipalityServiceImpl implements MunicipalityService {

    private MunicipalityDao municipalityDao;

    /**
     * Saves given municipality in the database and returns saved instance
     *
     * @param municipality
     * @return saved <code>Municipality</code> instance
     */
    @Override
    @Transactional
    public Municipality save(Municipality municipality) {

        Optional<Municipality> municipalityOptional = find(municipality.getCode());
        if (municipalityOptional.isPresent()) {
            log.info(String.format("Municipality with code %s already exists in the database", municipality.getCode()));

            return municipalityOptional.get();
        }

        Long id = municipalityDao.save(municipality);
        if (id == null) {
            throw new EntityNotSavedException(String.format("Municipality with code %s was not saved in the database",
                    municipality.getCode()));
        }
        municipality.setId(id);

        return municipality;
    }

    /**
     * Saves given list of municipalities and returns list of saved instances
     *
     * @param municipalities
     * @return <code>List</code> of saved <code>Municipality</code> instances
     */
    @Override
    @Transactional
    public List<Municipality> save(List<Municipality> municipalities) {

        if (municipalities.isEmpty()) {
            return Collections.emptyList();
        }

        municipalityDao.save(municipalities);

        List<Integer> codes = getCodes(municipalities);
        List<Municipality> savedMunicipalities = find(codes).stream()
                .filter(municipality -> municipality.getId() != null)
                .collect(Collectors.toList());
        if (savedMunicipalities.size() != municipalities.size()) {
            throw new EntityNotSavedException("Municipalities were not saved");
        }

        return savedMunicipalities;
    }

    /**
     * Finds municipality by its code
     *
     * @param municipalityCode
     * @return <code>Optional</code> of found municipality
     */
    @Override
    public Optional<Municipality> find(Integer municipalityCode) {

        return municipalityDao.find(municipalityCode);
    }

    /**
     * Finds municipalities by its codes
     *
     * @param municipalityCodes
     * @return <code>List</code> of found municipalities
     */
    @Override
    public List<Municipality> find(List<Integer> municipalityCodes) {

        return municipalityDao.find(municipalityCodes);
    }

    private List<Integer> getCodes(List<Municipality> municipalities) {

        return municipalities.stream()
                .map(Municipality::getCode)
                .collect(Collectors.toList());
    }
}

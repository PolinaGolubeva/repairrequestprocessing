package com.rrprocessing.components.request.controller;

import com.rrprocessing.components.request.domain.RepairRequest;
import com.rrprocessing.components.request.service.RequestService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * The <code>RequestController</code> processes http-requests getting required repair requests
 */
@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class RequestController {

    private RequestService requestService;

    /**
     * Finds repair requests by year and municipality code and returns list of found entities
     *
     * @param year
     * @param municipalityCode
     * @return list of found repair requests
     */
    @GetMapping("/year/{year}/municipality/{municipalityCode}")
    public List<RepairRequest> getByYearAndMunicipalityCode(@PathVariable Integer year,
                                                           @PathVariable Integer municipalityCode) {

        return requestService.find(year, municipalityCode);
    }
}

package com.rrprocessing.components.request.dao;

import com.rrprocessing.components.request.domain.RepairRequest;
import java.util.List;

/**
 * DAO working with <code>Request</code>
 */
public interface RequestDao {

    /**
     * Finds requests by year and municipality code
     *
     * @param year
     * @param municipalityCode
     * @return list of found requests
     */
    List<RepairRequest> findByYearAndMunicipality(Integer year, Integer municipalityCode);

    /**
     * Saves request to the database
     *
     * @param request value to be inserted
     * @return id of saved record
     */
    Long save(RepairRequest request);
}

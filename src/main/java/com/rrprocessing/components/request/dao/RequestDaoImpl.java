package com.rrprocessing.components.request.dao;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.components.request.domain.RepairRequest;
import com.rrprocessing.components.request.extractor.RepairRequestListExtractor;
import com.rrprocessing.exceptions.EntityNotSavedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Implementation of <code>RequestDao</code> working with <code>NamedParameterJdbcTemplate</code>
 */
@Repository
@Slf4j
public class RequestDaoImpl implements RequestDao {

    private final NamedParameterJdbcTemplate template;

    private static final String SELECT_REQUEST_QUERY = "SELECT request.id AS request_id, request_year, object_name, " +
            "address, area, declared_cost, is_request_repeated, municipality_id, " +
            "municipality.code AS municipality_code, " +
            "municipality.title AS municipality_title, " +
            "cadastral_number.id AS cadastral_id, " +
            "cadastral_number.cad_number AS cad_number, " +
            "work_type.title AS w_type, " +
            "additional_number.add_number AS add_number " +
            "FROM request " +
            "JOIN municipality ON municipality.id = municipality_id " +
            "LEFT JOIN request_cadastral_number ON request_cadastral_number.request_id = request.id " +
            "LEFT JOIN cadastral_number ON cadastral_number.id = request_cadastral_number.cadastral_id " +
            "LEFT JOIN work_type ON work_type.id = request.work_type_id " +
            "LEFT JOIN additional_number ON additional_number.id = request.additional_number_id " +
            "WHERE request_year = :year AND municipality.code = :municipalityCode " +
            "ORDER BY request.id";

    private static final String INSERT_REQUEST_QUERY = "INSERT INTO request (municipality_id, request_year, object_name, " +
            "address, work_type_id, additional_number_id, area, declared_cost, request_status_id, is_request_repeated) " +
            "VALUES (:municipalityId, :requestYear, :objectName, :address, :workTypeId, :additionalNumberId, :area, " +
            ":declaredCost, :requestStatusId, :isRequestRepeated)";
    private static final String INSERT_ADDITIONAL_NUMBER = "INSERT INTO additional_number (add_number) VALUES (:additionalNumber)";
    private static final String INSERT_REQUEST_CADASTRAL_NUMBER = "INSERT INTO request_cadastral_number " +
            "(request_id, cadastral_id) VALUES (:requestId, :cadastralId)";
    private static final String SELECT_WORK_TYPE_QUERY = "SELECT id FROM work_type WHERE title = :workTypeTitle";

    /**
     * Creates <code>RequestDaoImpl</code> object with configured template
     *
     * @param dataSource
     */
    @Autowired
    public RequestDaoImpl(DataSource dataSource) {

        this.template = new NamedParameterJdbcTemplate(dataSource);
    }

    /**
     * Finds requests by year and municipality code
     *
     * @param year
     * @param municipalityCode
     * @return list of found requests
     */
    @Override
    public List<RepairRequest> findByYearAndMunicipality(Integer year, Integer municipalityCode) {

        try {
            SqlParameterSource parameterSource = getParameterSource(year, municipalityCode);

            return template.query(SELECT_REQUEST_QUERY, parameterSource, new RepairRequestListExtractor());
        } catch (DataAccessException e) {
            log.warn("Requests by year {} and municipality code {} were not found", year, municipalityCode, e);

            return Collections.emptyList();
        }
    }

    /**
     * Saves request to the database
     *
     * @param request value to be inserted
     * @return id of saved record
     */
    @Override
    public Long save(RepairRequest request) {

        Long additionalNumberId = insertAdditionalNumber(request.getAdditionalNumber());
        Long workTypeId = getWorkTypeId(request.getWorkType());

        SqlParameterSource parameterSource = getAdditionalNumberParameterSource(request, additionalNumberId, workTypeId);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(INSERT_REQUEST_QUERY, parameterSource, keyHolder, new String[]{"id"});

        if (keyHolder.getKey() == null) {
            throw new EntityNotSavedException(String.format("Request %s was not saved", request));
        }
        Long id = Objects.requireNonNull(keyHolder.getKey()).longValue();
        insertRequestCadastralNumbers(request, id);

        return id;
    }

    private SqlParameterSource getParameterSource(Integer year, Integer municipalityCode) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("year", year);
        parameters.put("municipalityCode", municipalityCode);

        return new MapSqlParameterSource(parameters);
    }

    private Long insertAdditionalNumber(String additionalNumber) {

        if (additionalNumber != null) {
            SqlParameterSource parameterSource = new MapSqlParameterSource(Collections.singletonMap("additionalNumber", additionalNumber));
            KeyHolder keyHolder = new GeneratedKeyHolder();
            template.update(INSERT_ADDITIONAL_NUMBER, parameterSource, keyHolder, new String[]{"id"});

            if (keyHolder.getKey() == null) {
                throw new EntityNotSavedException(String.format("Additional number %s is not saved", additionalNumber));
            }

            return Objects.requireNonNull(keyHolder.getKey()).longValue();
        }

        return null;
    }

    private SqlParameterSource getAdditionalNumberParameterSource(RepairRequest request, Long additionalNumberId,
                                                                      Long workTypeId) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("municipalityId", request.getMunicipality().getId());
        parameters.put("requestYear", request.getYear());
        parameters.put("objectName", request.getObjectName());
        parameters.put("address", request.getAddress());
        parameters.put("workTypeId", workTypeId);
        parameters.put("additionalNumberId", additionalNumberId);
        parameters.put("declaredCost", request.getDeclaredCost());
        parameters.put("requestStatusId", request.getRequestStatus().getId());
        parameters.put("isRequestRepeated", request.getIsRequestRepeated());
        parameters.put("area", request.getArea());

        return new MapSqlParameterSource(parameters);
    }

    private Long getWorkTypeId(String workType) {

        if (workType != null) {
            SqlParameterSource parameterSource = new MapSqlParameterSource(Collections.singletonMap("workTypeTitle", workType));

            return template.queryForObject(SELECT_WORK_TYPE_QUERY, parameterSource, Long.class);
        }

        return null;
    }

    private void insertRequestCadastralNumbers(RepairRequest request, Long id) {

        if (!request.getCadastralNumbers().isEmpty()) {
            SqlParameterSource[] parameterSources = getCadastralParameterSources(request, id);
            template.batchUpdate(INSERT_REQUEST_CADASTRAL_NUMBER, parameterSources);
        }
    }

    private SqlParameterSource[] getCadastralParameterSources(RepairRequest request, Long id) {

        SqlParameterSource[] parameterSources = new SqlParameterSource[request.getCadastralNumbers().size()];

        for (int i = 0; i < request.getCadastralNumbers().size(); i++) {
            CadastralNumber number = request.getCadastralNumbers().get(i);
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("requestId", id);
            parameters.put("cadastralId", number.getId());
            parameterSources[i] = new MapSqlParameterSource(parameters);
        }

        return parameterSources;
    }
}

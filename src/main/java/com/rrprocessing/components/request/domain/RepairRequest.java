package com.rrprocessing.components.request.domain;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.components.municipality.domain.Municipality;
import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

/**
 * Entity representing repair request with corresponding dependencies
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RepairRequest {

    /**
     * Id of request record
     */
    private Long id;

    /**
     * Municipality that this request is assigned to
     */
    private Municipality municipality;

    /**
     * Year of this request creation
     */
    private Integer year;

    /**
     * Name of object to be repaired
     */
    private String objectName;

    /**
     * Address of object
     */
    private String address;

    /**
     * Name of required work type
     */
    private String workType;

    /**
     * List of cadastral numbers of objects assigned to this request
     */
    private List<CadastralNumber> cadastralNumbers;

    /**
     * Additional number of object if it's present
     */
    private String additionalNumber;

    /**
     * Area of assigned object
     */
    private Double area;

    /**
     * Declared cost of work
     */
    private Double declaredCost;

    /**
     * Status of this request
     */
    private RequestStatus requestStatus;

    /**
     * Flag indicating if this request was repeated
     */
    private Boolean isRequestRepeated;
}

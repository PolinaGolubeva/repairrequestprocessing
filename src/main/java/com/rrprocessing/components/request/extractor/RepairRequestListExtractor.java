package com.rrprocessing.components.request.extractor;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.components.cadastralnumber.mapper.CadastralNumberMapper;
import com.rrprocessing.components.request.domain.RepairRequest;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Extractor required to find data from <code>ResultSet</code> to convert to the <code>List</code>
 * of <code>RepairRequest</code>
 */
public class RepairRequestListExtractor implements ResultSetExtractor<List<RepairRequest>> {

    private final CadastralNumberMapper cadastralNumberMapper;
    private final RepairRequestMapper repairRequestMapper;
    private List<RepairRequest> repairRequests;

    /**
     * Creates <code>RepairRequestListExtractor</code> instance
     */
    public RepairRequestListExtractor() {

        this.cadastralNumberMapper = new CadastralNumberMapper();
        this.repairRequestMapper = new RepairRequestMapper();
    }

    /**
     * Extracts data from result set and converts to <code>List</code> of <code>RepairRequest</code>
     *
     * @param rs result set to be converted
     * @return list of resulting repair requests
     * @throws SQLException
     * @throws DataAccessException
     */
    @Override
    public List<RepairRequest> extractData(ResultSet rs) throws SQLException, DataAccessException {

        repairRequests = new ArrayList<>();

        while (rs.next()) {
            Long id = rs.getLong("request_id");
            RepairRequest request = getOrCreate(id, rs);
            CadastralNumber cadastralNumber = cadastralNumberMapper.mapRow(rs, rs.getRow());
            if (cadastralNumber != null && cadastralNumber.getNumber() != null) {
                request.getCadastralNumbers().add(cadastralNumber);
            }
        }

        return repairRequests;
    }

    private RepairRequest getOrCreate(Long id, ResultSet rs) throws SQLException {

        Optional<RepairRequest> requestOptional = findById(id);
        if (requestOptional.isEmpty()) {
            RepairRequest request = repairRequestMapper.mapRow(rs, rs.getRow());
            repairRequests.add(request);

            return request;
        }

        return requestOptional.get();
    }

    private Optional<RepairRequest> findById(Long id) {

        for (RepairRequest request : repairRequests) {
            if (request.getId().equals(id)) {
                return Optional.of(request);
            }
        }

        return Optional.empty();
    }
}

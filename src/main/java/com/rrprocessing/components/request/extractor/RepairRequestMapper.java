package com.rrprocessing.components.request.extractor;

import com.rrprocessing.components.municipality.domain.Municipality;
import com.rrprocessing.components.municipality.mapper.MunicipalityMapper;
import com.rrprocessing.components.request.domain.RepairRequest;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Mapper required to convert data from <code>ResultSet</code> row to <code>RepairRequest</code>
 */
public class RepairRequestMapper implements RowMapper<RepairRequest> {

    private final MunicipalityMapper municipalityMapper;

    /**
     * Creates <code>RepairRequestMapper</code> instance
     */
    public RepairRequestMapper() {

        this.municipalityMapper = new MunicipalityMapper();
    }

    /**
     * Maps <code>ResultSet</code> row to <code>RepairRequest</code>
     *
     * @param rs result set to be converted
     * @param rowNum index of current row in the result set
     * @return <code>Municipality</code> instance
     * @throws SQLException
     */
    @Override
    public RepairRequest mapRow(ResultSet rs, int rowNum) throws SQLException {

        RepairRequest request = new RepairRequest();
        request.setCadastralNumbers(new ArrayList<>());
        request.setId(rs.getLong("request_id"));
        request.setYear(rs.getInt("request_year"));
        request.setObjectName(rs.getString("object_name"));
        request.setAddress(rs.getString("address"));
        request.setArea(rs.getDouble("area"));
        request.setDeclaredCost(rs.getDouble("declared_cost"));
        request.setIsRequestRepeated(rs.getBoolean("is_request_repeated"));
        request.setWorkType(rs.getString("w_type"));
        request.setAdditionalNumber(rs.getString("add_number"));

        Municipality municipality = municipalityMapper.mapRow(rs, rowNum);
        request.setMunicipality(municipality);

        return request;
    }
}

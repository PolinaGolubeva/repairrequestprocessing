package com.rrprocessing.components.request.service;

import com.rrprocessing.components.request.domain.RepairRequest;
import java.util.List;

/**
 * The <code>RequestService</code> works with <code>RepairRequest</code> instances.
 * Provides with methods to retrieve required instances and to save given instances.
 */
public interface RequestService {

    /**
     * Searches for repair requests by given year and municipality code
     *
     * @param year
     * @param municipalityCode
     * @return <code>List</code> of found repair requests
     */
    List<RepairRequest> find(Integer year, Integer municipalityCode);

    /**
     * Saves given repair request
     *
     * @param request
     */
    void save(RepairRequest request);
}

package com.rrprocessing.components.request.service;

import com.rrprocessing.components.cadastralnumber.service.CadastralNumberService;
import com.rrprocessing.components.municipality.service.MunicipalityService;
import com.rrprocessing.components.requeststatus.service.RequestStatusService;
import com.rrprocessing.components.request.dao.RequestDao;
import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.components.municipality.domain.Municipality;
import com.rrprocessing.components.request.domain.RepairRequest;
import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import com.rrprocessing.exceptions.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of <code>RequestService</code>>
 */
@Service
@AllArgsConstructor
public class RequestServiceImpl implements RequestService {

    private RequestDao requestDao;
    private MunicipalityService municipalityService;
    private CadastralNumberService cadastralNumberService;
    private RequestStatusService requestStatusService;

    /**
     * Searches for repair requests by given year of request and municipality code
     *
     * @param year
     * @param municipalityCode
     * @return list of found repair requests
     */
    @Override
    public List<RepairRequest> find(Integer year, Integer municipalityCode) {

        List<RepairRequest> requests = requestDao.findByYearAndMunicipality(year, municipalityCode);

        for (RepairRequest request : requests) {
            Optional<RequestStatus> requestStatus = requestStatusService.find(request.getId());
            if (requestStatus.isEmpty()) {
                throw new NotFoundException(String.format("Request status for request with id %s is not found",
                        request.getId()));
            }

            request.setRequestStatus(requestStatus.get());
        }

        return requests;
    }

    /**
     * Saves given request in the database with corresponding entities
     * 
     * @param request
     */
    @Override
    @Transactional
    public void save(RepairRequest request) {

        Municipality municipality = request.getMunicipality();
        municipality = municipalityService.save(municipality);
        request.setMunicipality(municipality);

        List<CadastralNumber> cadastralNumbers = request.getCadastralNumbers();
        cadastralNumbers = cadastralNumberService.save(cadastralNumbers);
        request.setCadastralNumbers(cadastralNumbers);

        RequestStatus requestStatus = request.getRequestStatus();
        requestStatus = requestStatusService.save(requestStatus);
        request.setRequestStatus(requestStatus);

        requestDao.save(request);
    }
}

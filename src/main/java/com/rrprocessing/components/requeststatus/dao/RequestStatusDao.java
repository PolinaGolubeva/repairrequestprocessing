package com.rrprocessing.components.requeststatus.dao;

import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import java.util.Optional;

/**
 * DAO working with <code>RequestStatus</code>
 */
public interface RequestStatusDao {

    /**
     * Finds request status by request id if it exists
     *
     * @param requestId
     * @return <code>Optional</code> of found request status
     */
    Optional<RequestStatus> find(Long requestId);

    /**
     * Saves request status to the database
     *
     * @param requestStatus value to be saved
     * @return id of saved record
     */
    Long save(RequestStatus requestStatus);
}

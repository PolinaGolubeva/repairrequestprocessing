package com.rrprocessing.components.requeststatus.dao;

import com.rrprocessing.domain.ExecutionPeriod;
import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import com.rrprocessing.components.requeststatus.extractor.RequestStatusExtractor;
import com.rrprocessing.exceptions.EntityNotSavedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import javax.sql.DataSource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Implementation of <code>RequestStatusDao</code> working with <code>NamedParameterJdbcTemplate</code>
 */
@Repository
@Slf4j
public class RequestStatusDaoImpl implements RequestStatusDao {

    private final NamedParameterJdbcTemplate template;

    private static final String SELECT_QUERY = "SELECT request_status.id AS request_status_id, " +
            "is_rejected AS request_status_is_rejected, " +
            "comment.text AS request_status_comment, " +
            "included_cost AS request_status_included_cost, " +
            "execution_period.title AS execution_period_title, " +
            "request_execution.cost AS request_execution_cost  " +
            "FROM request_status " +
            "LEFT JOIN comment ON request_status.comment_id = comment.id " +
            "LEFT JOIN request_execution ON request_execution.request_status_id = request_status.id " +
            "LEFT JOIN execution_period ON execution_period.id = request_execution.execution_period_id " +
            "JOIN request ON request.request_status_id = request_status.id " +
            "WHERE request.id = :requestId";
    private static final String SELECT_EXECUTION_PERIOD = "SELECT id FROM execution_period WHERE title = :executionPeriodTitle";
    private static final String INSERT_COMMENT_QUERY = "INSERT INTO comment (text) VALUES (:commentText)";
    private static final String INSERT_REQUEST_STATUS = "INSERT INTO request_status " +
            "(is_rejected, comment_id, included_cost) VALUES (:isRejected, :commentId, :includedCost)";
    private static final String INSERT_REQUEST_EXECUTION_QUERY = "INSERT INTO request_execution " +
            "(execution_period_id, cost, request_status_id) VALUES (:executionPeriodId, :cost, :requestStatusId)";

    /**
     * Creates <code>RequestStatusDaoImpl</code> object with configured template
     *
     * @param dataSource
     */
    @Autowired
    public RequestStatusDaoImpl(DataSource dataSource) {

        this.template = new NamedParameterJdbcTemplate(dataSource);
    }

    /**
     * Finds request status by request id
     *
     * @param requestId
     * @return <code>Optional</code> of found request status record
     */
    @Override
    public Optional<RequestStatus> find(Long requestId) {

        RequestStatus requestStatus = null;
        try {
            requestStatus = template.query(SELECT_QUERY, Collections.singletonMap("requestId", requestId),
                    new RequestStatusExtractor());
        } catch (DataAccessException e) {
            log.warn("Request status for request with id {} was not found", requestId, e);
        }

        return Optional.ofNullable(requestStatus);
    }

    /**
     * Saves request status to the database
     *
     * @param requestStatus value to be saved
     * @return id of saved record
     */
    @Override
    public Long save(RequestStatus requestStatus) {

        Long commentId = insertComment(requestStatus.getComment());
        Long requestStatusId = insertRequestStatus(requestStatus, commentId);
        saveRequestExecutions(requestStatus, requestStatusId);

        return requestStatusId;
    }

    private Long insertComment(String comment) {

        if (comment != null) {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            SqlParameterSource parameterSource = new MapSqlParameterSource(Collections.singletonMap("commentText", comment));
            template.update(INSERT_COMMENT_QUERY, parameterSource, keyHolder, new String[]{"id"});

            if (keyHolder.getKey() == null) {
                throw new EntityNotSavedException(String.format("Comment %s for request status was not saved",
                        comment));
            }

            return Objects.requireNonNull(keyHolder.getKey()).longValue();
        }

        return null;
    }

    private Long insertRequestStatus(RequestStatus requestStatus, Long commentId) {

        SqlParameterSource parameterSource = getRequestStatusParameters(requestStatus, commentId);
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(INSERT_REQUEST_STATUS, parameterSource, keyHolder, new String[]{"id"});

        if (keyHolder.getKey() == null) {
            throw new EntityNotSavedException(String.format("Request status %s was not saved", requestStatus));
        }

        return Objects.requireNonNull(keyHolder.getKey()).longValue();
    }

    private void saveRequestExecutions(RequestStatus requestStatus, Long requestStatusId) {

        if (!requestStatus.getExecutionCosts().isEmpty()) {
            SqlParameterSource[] parameters = getRequestExecutionParameters(requestStatus.getExecutionCosts(), requestStatusId);
            template.batchUpdate(INSERT_REQUEST_EXECUTION_QUERY, parameters);
        }
    }

    private SqlParameterSource getRequestStatusParameters(RequestStatus requestStatus, Long commentId) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("isRejected", requestStatus.getIsRejected());
        parameters.put("commentId", commentId);
        parameters.put("includedCost", requestStatus.getIncludedCost());

        return new MapSqlParameterSource(parameters);
    }

    private SqlParameterSource[] getRequestExecutionParameters(Map<ExecutionPeriod, Double> executionCosts,
                                                                    Long requestStatusId) {

        Map<ExecutionPeriod, Long> executionPeriodIds = getExecutionPeriodIds(executionCosts.keySet());

        SqlParameterSource[] parameterSources = new SqlParameterSource[executionCosts.size()];
        int i = 0;
        for (Map.Entry<ExecutionPeriod, Double> entry : executionCosts.entrySet()) {
            Long executionPeriodId = executionPeriodIds.get(entry.getKey());
            Double cost = entry.getValue();
            parameterSources[i] = getRequestExecutionParameterSource(requestStatusId, executionPeriodId, cost);
            i++;
        }

        return parameterSources;
    }

    private SqlParameterSource getRequestExecutionParameterSource(Long requestStatusId, Long executionPeriodId,
                                                                  Double cost) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("requestStatusId", requestStatusId);
        parameters.put("executionPeriodId", executionPeriodId);
        parameters.put("cost", cost);

        return new MapSqlParameterSource(parameters);
    }

    private Map<ExecutionPeriod, Long> getExecutionPeriodIds(Set<ExecutionPeriod> executionPeriods) {

        Map<ExecutionPeriod, Long> executionPeriodIds = new HashMap<>();
        for (ExecutionPeriod executionPeriod : executionPeriods) {
            Long periodId = executionPeriodIds.computeIfAbsent(executionPeriod, this::getExecutionPeriodId);
            executionPeriodIds.put(executionPeriod, periodId);
        }

        return executionPeriodIds;
    }

    private Long getExecutionPeriodId(ExecutionPeriod executionPeriod) {

        return template.queryForObject(SELECT_EXECUTION_PERIOD,
                Collections.singletonMap("executionPeriodTitle", executionPeriod.getTitle()), Long.class);
    }
}

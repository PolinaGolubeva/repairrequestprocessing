package com.rrprocessing.components.requeststatus.domain;

import com.rrprocessing.domain.ExecutionPeriod;
import lombok.Data;
import java.util.Map;

/**
 * Entity representing status of request with additional information
 */
@Data
public class RequestStatus {

    /**
     * Id of request status record
     */
    private Long id;

    /**
     * Flag indicating if request is rejected
     */
    private Boolean isRejected;

    /**
     * Comment clarifying why request was rejected
     */
    private String comment;

    /**
     * Included cost of work for this request
     */
    private Double includedCost;

    /**
     * Map of execution periods and corresponding costs of work, representing work plan
     */
    private Map<ExecutionPeriod, Double> executionCosts;
}

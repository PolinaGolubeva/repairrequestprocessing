package com.rrprocessing.components.requeststatus.extractor;

import com.rrprocessing.domain.ExecutionPeriod;
import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Extractor required to find data from <code>ResultSet</code> to convert to <code>RequestStatus</code>
 */
public class RequestStatusExtractor implements ResultSetExtractor<RequestStatus> {

    /**
     * Extracts data from result set and converts to <code>RequestStatus</code>
     *
     * @param rs result set to be converted
     * @return resulting request status
     * @throws SQLException
     * @throws DataAccessException
     */
    @Override
    public RequestStatus extractData(ResultSet rs) throws SQLException, DataAccessException {

        rs.next();

        RequestStatus requestStatus = new RequestStatus();
        requestStatus.setId(rs.getLong("request_status_id"));
        requestStatus.setComment(rs.getString("request_status_comment"));
        requestStatus.setIsRejected(rs.getBoolean("request_status_is_rejected"));
        requestStatus.setIncludedCost(rs.getDouble("request_status_included_cost"));
        requestStatus.setExecutionCosts(extractExecutionCosts(rs));

        return requestStatus;
    }

    private Map<ExecutionPeriod, Double> extractExecutionCosts(ResultSet rs) throws SQLException {

        Map<ExecutionPeriod, Double> executionCosts = new HashMap<>();

        do {
            String executionPeriodTitle = rs.getString("execution_period_title");
            if (executionPeriodTitle != null) {
                ExecutionPeriod executionPeriod = ExecutionPeriod.get(executionPeriodTitle);
                Double cost = rs.getDouble("request_execution_cost");
                executionCosts.put(executionPeriod, cost);
            }
        } while (rs.next());

        return executionCosts;
    }
}

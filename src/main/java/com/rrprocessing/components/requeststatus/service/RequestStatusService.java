package com.rrprocessing.components.requeststatus.service;

import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import java.util.Optional;

/**
 * The <code>RequestStatusService</code> works with <code>RequestStatus</code> instances.
 * Provides with methods to retrieve required instances and to save given instances.
 */
public interface RequestStatusService {

    /**
     * Finds request status associated with repair request by request's id
     *
     * @param requestId
     * @return <code>Optional</code> of found <code>RequestStatus</code> instance
     */
    Optional<RequestStatus> find(Long requestId);

    /**
     * Saves given request status in the database and returns saved instance
     *
     * @param requestStatus
     * @return saved <code>RequestStatus</code> instance
     */
    RequestStatus save(RequestStatus requestStatus);
}

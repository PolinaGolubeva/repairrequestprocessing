package com.rrprocessing.components.requeststatus.service;

import com.rrprocessing.components.requeststatus.dao.RequestStatusDao;
import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;

/**
 * Implementation of <code>RequestStatusService</code>
 */
@Service
@AllArgsConstructor
public class RequestStatusServiceImpl implements RequestStatusService {

    private RequestStatusDao requestStatusDao;

    /**
     * Saves given request status in the database if it's not null and returns saved instance
     *
     * @param requestStatus
     * @return saved <code>RequestStatus</code> instance
     */
    @Override
    @Transactional
    public RequestStatus save(RequestStatus requestStatus) {

        if (requestStatus != null) {
            Long id = requestStatusDao.save(requestStatus);
            requestStatus.setId(id);

            return requestStatus;
        }

        return null;
    }

    /**
     * Finds request status associated with repair request by request's id
     *
     * @param requestId
     * @return <code>Optional</code> of found <code>RequestStatus</code> instance
     */
    @Override
    public Optional<RequestStatus> find(Long requestId) {

        return requestStatusDao.find(requestId);
    }
}

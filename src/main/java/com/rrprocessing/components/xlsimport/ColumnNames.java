package com.rrprocessing.components.xlsimport;

import lombok.Getter;
import java.util.Arrays;
import java.util.Optional;

/**
 * The <code>ColumnNames</code> contains expected names of columns in the xlsx-file to be read
 */
public enum ColumnNames {

    MUNICIPALITY_CODE("№ МО"),
    MUNICIPALITY_TITLE("Наименование муниципального образования"),
    YEAR("Год планируемого ремонта"),
    OBJECT_NAME("Наименование объекта недвижимого имущества, подлежащего ремонту"),
    ADDRESS("Адрес месторасположения"),
    CADASTRAL_NUMBER("Кадастровый номер"),
    ADDITIONAL_NUMBER("Иной учетный номер (при наличии)"),
    AREA("Площадь объекта (кв. м.)"),
    DECLARED_COST("Заявлено"),
    INCLUDED_COST("Включено"),
    REJECTION_REASON("Причина отказа"),
    REPEATED_REQUEST("Отметка о повторной заявке"),
    FIRST_QUARTER("1 кв"),
    SECOND_QUARTER("2 кв"),
    THIRD_QUARTER("3 кв"),
    FOURTH_QUARTER("4 кв");

    @Getter
    private final String title;

    ColumnNames(String title) {
        this.title = title;
    }

    /**
     * Finds <code>ColumnNames</code> instance by its title
     *
     * @param title
     * @return <code>ColumnNames</code> instance with given title
     */
    public static ColumnNames get(String title) {

        Optional<ColumnNames> result = Arrays.stream(ColumnNames.values())
                .filter(dbColumn -> dbColumn.title.equals(title))
                .findFirst();

        return result.orElse(null);
    }
}

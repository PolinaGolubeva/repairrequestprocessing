package com.rrprocessing.components.xlsimport.converter;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.components.municipality.domain.Municipality;
import com.rrprocessing.components.request.domain.RepairRequest;
import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import com.rrprocessing.components.xlsimport.dto.RowData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * The <code>RowDataToRepairRequestConverter</code> is used to convert <code>RowData</code> objects
 * to <code>RepairRequest</code> objects
 */
@Component
public class RowDataToRepairRequestConverter implements Converter<RowData, RepairRequest> {

    /**
     * Converts row data to repair request
     *
     * @param rowData
     * @return resulting <code>RepairRequest</code> object
     */
    public RepairRequest convert(RowData rowData) {

        RepairRequest request = new RepairRequest();
        request.setYear(rowData.getYear());
        request.setObjectName(rowData.getObjectName());
        request.setAddress(rowData.getAddress());
        request.setArea(rowData.getArea());
        request.setDeclaredCost(rowData.getDeclaredCost());
        request.setIsRequestRepeated(rowData.isRepeated());
        request.setAdditionalNumber(rowData.getAdditionalNumber());
        request.setWorkType("Ремонт");

        Municipality municipality = extractMunicipality(rowData);
        request.setMunicipality(municipality);

        List<CadastralNumber> cadastralNumbers = extractCadastralNumbers(rowData);
        request.setCadastralNumbers(cadastralNumbers);

        RequestStatus requestStatus = extractRequestStatus(rowData);
        request.setRequestStatus(requestStatus);

        return request;
    }

    private Municipality extractMunicipality(RowData rowData) {

        Municipality municipality = new Municipality();
        municipality.setTitle(rowData.getMunicipalityTitle());
        municipality.setCode(rowData.getMunicipalityCode());

        return municipality;
    }

    private List<CadastralNumber> extractCadastralNumbers(RowData rowData) {

        List<CadastralNumber> cadastralNumbers = new ArrayList<>();
        if (rowData.getCadastralNumbers() != null) {
            String[] cadastralNumberStrings = rowData.getCadastralNumbers().split("\n");

            for (String s : cadastralNumberStrings) {
                CadastralNumber number = new CadastralNumber();
                number.setNumber(s);
                cadastralNumbers.add(number);
            }
        }

        return cadastralNumbers;
    }

    private RequestStatus extractRequestStatus(RowData rowData) {

        RequestStatus requestStatus = null;

        if (rowData.getIncludedCost() != null) {
            requestStatus = new RequestStatus();
            requestStatus.setIncludedCost(rowData.getIncludedCost());
            requestStatus.setIsRejected(rowData.getIncludedCost() == 0.0);
            requestStatus.setComment(rowData.getRejectionReason());
            requestStatus.setExecutionCosts(rowData.getExecutionCosts());
        }

        return requestStatus;
    }
}

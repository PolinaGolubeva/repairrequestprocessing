package com.rrprocessing.components.xlsimport.dto;

import com.rrprocessing.domain.ExecutionPeriod;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * The <code>RowData</code> object contains information acquired from one row of xlsx-file
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RowData {

    /**
     * Code of municipality
     */
    private Integer municipalityCode;

    /**
     * Title of municipality
     */
    private String municipalityTitle;

    /**
     * Year of request creation
     */
    private Integer year;

    /**
     * Name of object mentioned in the request
     */
    private String objectName;

    /**
     * Address of the object
     */
    private String address;

    /**
     * Cadastral number or numbers of the object
     */
    private String cadastralNumbers;

    /**
     * Additional number of the object if present
     */
    private String additionalNumber;

    /**
     * Area of the object
     */
    private Double area;

    /**
     * Declared cost of required works
     */
    private Double declaredCost;

    /**
     * Included cost of works
     */
    private Double includedCost;

    /**
     * Rejection reason or comment
     */
    private String rejectionReason;

    /**
     * Flag indicating if this request was repeated
     */
    private boolean isRepeated;

    /**
     * Map of execution periods and corresponding costs of work, represents work plan
     */
    private Map<ExecutionPeriod, Double> executionCosts;
}

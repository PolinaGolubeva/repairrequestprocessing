package com.rrprocessing.components.xlsimport.listener;

import com.rrprocessing.components.xlsimport.service.XlsImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * The <code>ImportListener</code> start data import from xlsx-file with file name
 * specified as import.filename property if import is enabled via import.enable property
 */
@Component
@ConditionalOnProperty(prefix = "import", name = "enable", havingValue = "true")
public class ImportListener {

    private final XlsImportService xlsImportService;

    @Autowired
    public ImportListener(XlsImportService xlsImportService) {
        this.xlsImportService = xlsImportService;
    }

    @Value("${import.filename}")
    private String fileName;

    /**
     * Launches import process when application context is ready
     */
    @EventListener(ApplicationReadyEvent.class)
    public void importDataFromXlsxFile() {

        xlsImportService.importData(fileName);
    }
}

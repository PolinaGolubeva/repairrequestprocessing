package com.rrprocessing.components.xlsimport.reader;

import com.rrprocessing.domain.ExecutionPeriod;
import com.rrprocessing.components.xlsimport.ColumnNames;
import com.rrprocessing.components.xlsimport.dto.RowData;
import com.rrprocessing.exceptions.XlsReadingException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;

/**
 * The <code>XlsReader</code> object provides with method to read xlsx-file with given name
 * and extract data to the <code>RowData</code> list
 */
@Component
public class XlsReader {

    private FormulaEvaluator formulaEvaluator;
    private Sheet sheet;
    private List<CellRangeAddress> mergedRegions;
    private Map<Integer, ColumnNames> columns;
    private Map<ColumnNames, BiConsumer<Cell, RowData>> columnRules;

    /**
     * Reads file with given file name and extracts data to the <code>RowData</code> list
     *
     * @param fileName
     * @return <code>List</code> of extracted <code>RowData</code> objects
     */
    public List<RowData> extractData(String fileName) {

        try (FileInputStream file = new FileInputStream(fileName)) {

            initReader(file);
            readHeader();
            initRules();

            return readRowData();
        } catch (IOException e) {
            throw new XlsReadingException("Can not read xls-file " + fileName, e);
        }
    }

    private void initReader(FileInputStream file) throws IOException {

        XSSFWorkbook workbook = new XSSFWorkbook(file);
        workbook.close();
        formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
        sheet = workbook.getSheetAt(0);
        mergedRegions = sheet.getMergedRegions();
    }

    private void readHeader() {

        columns = new HashMap<>();

        int firstRow = sheet.getFirstRowNum();
        Row header1 = sheet.getRow(firstRow);
        Row header2 = sheet.getRow(firstRow + 1);

        Iterator<Cell> headerIterator1 = header1.cellIterator();
        Iterator<Cell> headerIterator2 = header2.cellIterator();

        while (headerIterator1.hasNext() && headerIterator2.hasNext()) {
            Cell currentCell1 = headerIterator1.next();
            Cell currentCell2 = headerIterator2.next();

            String colName;
            ColumnNames column;
            if (CellType.BLANK.equals(currentCell1.getCellType())
                    || ColumnNames.get(currentCell1.getStringCellValue()) == null) {
                colName = currentCell2.getStringCellValue();
            }
            else {
                colName = currentCell1.getStringCellValue();
            }
            column = ColumnNames.get(colName);

            if (column != null) {
                columns.put(currentCell1.getColumnIndex(), column);
            }
        }
    }

    private void initRules() {

        columnRules = new HashMap<>();
        columnRules.put(ColumnNames.MUNICIPALITY_CODE, this::extractMunicipalityCode);
        columnRules.put(ColumnNames.MUNICIPALITY_TITLE, this::extractMunicipalityTitle);
        columnRules.put(ColumnNames.YEAR, this::extractYear);
        columnRules.put(ColumnNames.OBJECT_NAME, this::extractObjectName);
        columnRules.put(ColumnNames.ADDRESS, this::extractAddress);
        columnRules.put(ColumnNames.CADASTRAL_NUMBER, this::extractCadastralNumber);
        columnRules.put(ColumnNames.ADDITIONAL_NUMBER, this::extractAdditionalNumber);
        columnRules.put(ColumnNames.AREA, this::extractArea);
        columnRules.put(ColumnNames.INCLUDED_COST, this::extractIncludedCost);
        columnRules.put(ColumnNames.DECLARED_COST, this::extractDeclaredCost);
        columnRules.put(ColumnNames.REJECTION_REASON, this::extractRejectionReason);
        columnRules.put(ColumnNames.REPEATED_REQUEST, this::extractRepeated);
        columnRules.put(ColumnNames.FIRST_QUARTER, this::extractExecutionCost);
        columnRules.put(ColumnNames.SECOND_QUARTER, this::extractExecutionCost);
        columnRules.put(ColumnNames.THIRD_QUARTER, this::extractExecutionCost);
        columnRules.put(ColumnNames.FOURTH_QUARTER, this::extractExecutionCost);
    }

    private List<RowData> readRowData() {

        List<RowData> data = new ArrayList<>();

        // skip 3 header lines
        int rowIndex = 3;
        Row row = sheet.getRow(rowIndex);

        int yearColumnIndex = getYearColumnIndex();
        Cell yearCell = row.getCell(yearColumnIndex);
        while (!yearCell.getCellType().equals(CellType.BLANK)) {

            RowData rowData = new RowData();
            rowData.setExecutionCosts(new HashMap<>());

            Row finalRow = row;
            columns.keySet().forEach(columnIndex -> {
                Cell cell = finalRow.getCell(columnIndex);
                ColumnNames column = columns.get(columnIndex);
                BiConsumer<Cell, RowData> rule = columnRules.get(column);
                rule.accept(cell, rowData);
            });

            // check if requests must be merged
            if (rowData.getDeclaredCost() == null && checkRowDataIsPartOfRequest(row)) {
                RowData previousRowData = data.get(data.size() - 1);
                mergeRowData(previousRowData, rowData);
            } else {
                data.add(rowData);
            }

            rowIndex++;
            row = sheet.getRow(rowIndex);
            yearCell = row.getCell(yearColumnIndex);
        }

        return data;
    }

    void extractMunicipalityCode(Cell cell, RowData data) {

        String code = null;
        if (cell.getCellType().equals(CellType.STRING)) {
            code = cell.getStringCellValue();
        } else if (cell.getCellType().equals(CellType.BLANK)) {
            code = extractMergedRegionStringData(cell);
        } else if (cell.getCellType().equals(CellType.NUMERIC)) {
            code = Integer.toString((int) cell.getNumericCellValue());
        }

        if (code != null) {
            data.setMunicipalityCode(Integer.valueOf(code));
        }
    }

    void extractMunicipalityTitle(Cell cell, RowData data) {

        String title = null;
        if (cell.getCellType().equals(CellType.STRING)) {
            title = cell.getStringCellValue();
        }
        if (cell.getCellType().equals(CellType.BLANK)) {
            title = extractMergedRegionStringData(cell);
        }

        data.setMunicipalityTitle(title);
    }

    void extractYear(Cell cell, RowData data) {

        if (cell.getCellType().equals(CellType.STRING)) {
            String year = cell.getStringCellValue();
            data.setYear(Integer.valueOf(year));
        }
        if (cell.getCellType().equals(CellType.NUMERIC)) {
            double year = cell.getNumericCellValue();
            data.setYear((int) year);
        }
    }

    void extractObjectName(Cell cell, RowData data) {

        String objectName = null;
        if (cell.getCellType().equals(CellType.STRING)) {
            objectName = cell.getStringCellValue();
        }
        if (cell.getCellType().equals(CellType.BLANK)) {
            objectName = extractMergedRegionStringData(cell);
        }

        data.setObjectName(objectName);
    }

    void extractAddress(Cell cell, RowData data) {

        String address = cell.getStringCellValue();
        data.setAddress(address);
    }

    void extractCadastralNumber(Cell cell, RowData data) {

        String cadastralNumber = cell.getStringCellValue();
        if (!"нет".equals(cadastralNumber)) {
            data.setCadastralNumbers(cadastralNumber);
        }
    }

    void extractAdditionalNumber(Cell cell, RowData data) {

        if (!cell.getCellType().equals(CellType.BLANK)) {
            String additionalNumber = cell.getStringCellValue();
            if (!"нет".equals(additionalNumber)) {
                data.setAdditionalNumber(additionalNumber);
            }
        }
    }

    void extractArea(Cell cell, RowData data) {

        if (cell.getCellType().equals(CellType.NUMERIC)) {
            double area = cell.getNumericCellValue();
            data.setArea(area);
        }

        if (cell.getCellType().equals(CellType.STRING)) {
            String area = cell.getStringCellValue();
            area = area.replace(",", ".");
            data.setArea(Double.parseDouble(area));
        }
    }

    void extractDeclaredCost(Cell cell, RowData data) {

        if (cell.getCellType().equals(CellType.NUMERIC)) {
            double cost = cell.getNumericCellValue();
            data.setDeclaredCost(cost);
        }

        if (cell.getCellType().equals(CellType.FORMULA)) {
            CellValue result = formulaEvaluator.evaluate(cell);
            double cost = result.getNumberValue();
            data.setDeclaredCost(cost);
        }
    }

    void extractIncludedCost(Cell cell, RowData data) {

        if (cell.getCellType().equals(CellType.NUMERIC)) {
            double cost = cell.getNumericCellValue();
            data.setIncludedCost(cost);
        }

        if (cell.getCellType().equals(CellType.BLANK)) {
            Optional<CellRangeAddress> mergeRegion = getMergedRegionByCell(cell);
            if (mergeRegion.isEmpty()) {
                data.setIncludedCost(0.0);
            }
        }

        if (cell.getCellType().equals(CellType.FORMULA)) {
            CellValue result = formulaEvaluator.evaluate(cell);
            double cost = result.getNumberValue();
            data.setIncludedCost(cost);
        }
    }

    void extractRejectionReason(Cell cell, RowData data) {

        if (!cell.getCellType().equals(CellType.BLANK)) {
            String reason = cell.getStringCellValue();
            data.setRejectionReason(reason);
        }
    }

    void extractRepeated(Cell cell, RowData data) {

        data.setRepeated(!cell.getCellType().equals(CellType.BLANK));
    }

    void extractExecutionCost(Cell cell, RowData data) {

        if (!cell.getCellType().equals(CellType.BLANK)) {
            int columnIndex = cell.getColumnIndex();
            String columnName = columns.get(columnIndex).getTitle();
            ExecutionPeriod executionPeriod = ExecutionPeriod.get(columnName);
            double cost = cell.getNumericCellValue();

            data.getExecutionCosts().put(executionPeriod, cost);
        }
    }

    private int getYearColumnIndex() {

        Optional<Integer> yearColumnOptional = columns.entrySet().stream()
                .filter(entry -> entry.getValue().equals(ColumnNames.YEAR))
                .map(Map.Entry::getKey)
                .findFirst();
        if (yearColumnOptional.isEmpty()) {
            throw new IllegalStateException("Table must have year column");
        }

        return yearColumnOptional.get();
    }

    private boolean checkRowDataIsPartOfRequest(Row row) {

        Optional<Integer> declaredCostColumnOptional = columns.entrySet().stream()
                .filter(entry -> entry.getValue().equals(ColumnNames.DECLARED_COST))
                .map(Map.Entry::getKey)
                .findFirst();
        if (declaredCostColumnOptional.isEmpty()) {
            throw new IllegalStateException("Table must have declared cost column");
        }

        int declaredCostColumnIndex = declaredCostColumnOptional.get();
        Cell declaredCostCell = row.getCell(declaredCostColumnIndex);

        return getMergedRegionByCell(declaredCostCell).isPresent();
    }

    private void mergeRowData(RowData previousRowData, RowData rowData) {

        previousRowData.setObjectName(mergeStrings(previousRowData.getObjectName(), rowData.getObjectName()));
        previousRowData.setAddress(mergeStrings(previousRowData.getAddress(), rowData.getAddress()));
        previousRowData.setCadastralNumbers(mergeStrings(previousRowData.getCadastralNumbers(), rowData.getCadastralNumbers()));
        previousRowData.setAdditionalNumber(mergeStrings(previousRowData.getAdditionalNumber(), rowData.getAdditionalNumber()));
        previousRowData.setArea(previousRowData.getArea() + rowData.getArea());
    }

    private String mergeStrings(String s1, String s2) {

        if (s1 == null) {
            return s2;
        } else {
            if (s2 == null)
                return s1;
            return s1 + "\n" + s2;
        }
    }

    private String extractMergedRegionStringData(Cell cell) {

        String data = null;
        Optional<CellRangeAddress> mergedRegion = getMergedRegionByCell(cell);
        if (mergedRegion.isPresent()) {
            Cell firstCell = getFirstCell(mergedRegion.get());
            data = firstCell.getStringCellValue();
        }

        return data;
    }

    private Optional<CellRangeAddress> getMergedRegionByCell(Cell cell) {

        for (CellRangeAddress cellAddress : mergedRegions) {

            if(cellAddress.containsColumn(cell.getColumnIndex()) && cellAddress.containsRow(cell.getRowIndex())) {
                return Optional.of(cellAddress);
            }
        }

        return Optional.empty();
    }

    private Cell getFirstCell(CellRangeAddress cellRangeAddress) {

        int rowIndex = cellRangeAddress.getFirstRow();
        int columnIndex = cellRangeAddress.getFirstColumn();

        return sheet.getRow(rowIndex).getCell(columnIndex);
    }
}

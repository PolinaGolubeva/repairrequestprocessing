package com.rrprocessing.components.xlsimport.service;

/**
 * The <code>XlsImportService</code> provides with methods to read and save data from the xlsx-file
 */
public interface XlsImportService {

    /**
     * Reads and saves data from xlsx-file with given name
     *
     * @param fileName
     */
    void importData(String fileName);
}

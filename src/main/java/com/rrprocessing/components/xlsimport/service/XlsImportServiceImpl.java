package com.rrprocessing.components.xlsimport.service;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.components.cadastralnumber.service.CadastralNumberService;
import com.rrprocessing.components.request.dao.RequestDao;
import com.rrprocessing.components.municipality.domain.Municipality;
import com.rrprocessing.components.request.domain.RepairRequest;
import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import com.rrprocessing.exceptions.NotFoundException;
import com.rrprocessing.components.municipality.service.MunicipalityService;
import com.rrprocessing.components.requeststatus.service.RequestStatusService;
import com.rrprocessing.components.xlsimport.dto.RowData;
import com.rrprocessing.components.xlsimport.converter.RowDataToRepairRequestConverter;
import com.rrprocessing.components.xlsimport.reader.XlsReader;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation of <code>XlsImportService</code>. The bean is created if the import is enabled via
 * import.enable property
 */
@Service
@AllArgsConstructor
@Slf4j
@ConditionalOnProperty(prefix = "import", name = "enable", havingValue = "true")
public class XlsImportServiceImpl implements XlsImportService {

    private XlsReader reader;
    private RowDataToRepairRequestConverter converter;
    private RequestDao requestDao;
    private MunicipalityService municipalityService;
    private CadastralNumberService cadastralNumberService;
    private RequestStatusService requestStatusService;

    /**
     * Reads xlsx-file with given file name, converts data to <code>RepairRequest</code> instances
     * and saves acquired data to the database
     *
     * @param fileName
     */
    @Override
    @Transactional
    public void importData(String fileName) {

        List<RowData> data = reader.extractData(fileName);
        log.info("Data was extracted from the file {}", fileName);

        List<RepairRequest> requests = convertToRequests(data);
        List<RepairRequest> filteredRequests = filterExistingRepairRequests(requests);

        saveMunicipalities(filteredRequests);
        saveCadastralNumbers(filteredRequests);
        saveRequests(filteredRequests);
    }

    List<RepairRequest> convertToRequests(List<RowData> data) {

        return data.stream().map(rowData -> converter.convert(rowData))
                .collect(Collectors.toList());
    }

    List<RepairRequest> filterExistingRepairRequests(List<RepairRequest> requests) {

        List<RepairRequest> filteredRequests = new ArrayList<>();
        for (RepairRequest request : requests) {
            Integer year = request.getYear();
            Integer municipalityCode = request.getMunicipality().getCode();

            List<RepairRequest> foundRequests = requestDao.findByYearAndMunicipality(year, municipalityCode);
            Optional<RepairRequest> existingRequest = foundRequests.stream()
                    .filter(found -> areRequestsTheSame(found, request))
                    .findFirst();
            if (existingRequest.isEmpty()) {
                filteredRequests.add(request);
            }
        }

        return filteredRequests;
    }

    private boolean areRequestsTheSame(RepairRequest request1, RepairRequest request2) {

        Integer municipalityCode1 = request1.getMunicipality().getCode();
        Integer municipalityCode2 = request2.getMunicipality().getCode();

        return request1.getYear().equals(request2.getYear())
                && municipalityCode1.equals(municipalityCode2)
                && Objects.equals(request1.getObjectName(), request2.getObjectName())
                && Objects.equals(request1.getAddress(), request2.getAddress())
                && Objects.equals(request1.getAdditionalNumber(), request2.getAdditionalNumber());
    }


    void saveMunicipalities(List<RepairRequest> requests) {

        List<Municipality> municipalities = requests.stream()
                .map(RepairRequest::getMunicipality)
                .distinct()
                .collect(Collectors.toList());
        municipalities = municipalityService.save(municipalities);

        for (RepairRequest request : requests) {
            Optional<Municipality> correctMunicipality = municipalities.stream()
                    .filter(municipality -> request.getMunicipality().getCode().equals(municipality.getCode()))
                    .findFirst();

            if (correctMunicipality.isEmpty()) {
                Integer municipalityCode = request.getMunicipality().getCode();
                throw new NotFoundException(String.format("Municipality with code %s is not found", municipalityCode));
            }

            request.setMunicipality(correctMunicipality.get());
        }

        log.info("Municipalities were successfully saved");
    }

    void saveCadastralNumbers(List<RepairRequest> requests) {

        List<CadastralNumber> cadastralNumbers = requests.stream()
                .flatMap(request -> request.getCadastralNumbers().stream())
                .distinct()
                .collect(Collectors.toList());
        cadastralNumbers = cadastralNumberService.save(cadastralNumbers);

        for (RepairRequest request : requests) {

            List<String> oldCadastralNumbers = request.getCadastralNumbers().stream()
                    .map(CadastralNumber::getNumber)
                    .collect(Collectors.toList());
            List<CadastralNumber> newNumbers = cadastralNumbers.stream()
                    .filter(cadastralNumber -> oldCadastralNumbers.contains(cadastralNumber.getNumber()))
                    .collect(Collectors.toList());

            request.setCadastralNumbers(newNumbers);
        }

        log.info("Cadastral numbers were successfully saved");
    }

    void saveRequests(List<RepairRequest> requests) {

        for (int i = 0; i < requests.size(); i++) {
            RepairRequest currentRequest = requests.get(i);

            if (currentRequest.getRequestStatus() != null) {
                RequestStatus requestStatus = requestStatusService.save(currentRequest.getRequestStatus());
                currentRequest.setRequestStatus(requestStatus);
                requestDao.save(currentRequest);
            } else {
                currentRequest.setRequestStatus(requests.get(i-1).getRequestStatus());
                requestDao.save(currentRequest);
            }
        }

        log.info("Repair requests were successfully saved");
    }
}

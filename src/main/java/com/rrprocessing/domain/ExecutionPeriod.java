package com.rrprocessing.domain;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import java.util.Arrays;
import java.util.Optional;

/**
 * Time periods in which requests can be processed
 */
public enum ExecutionPeriod {
    FIRST_QUARTER("1 кв"),
    SECOND_QUARTER("2 кв"),
    THIRD_QUARTER("3 кв"),
    FOURTH_QUARTER("4 кв");

    /**
     * Title of current execution period
     */
    @Getter
    @JsonValue
    private final String title;

    ExecutionPeriod(String title) {
        this.title = title;
    }

    /**
     * Returns <code>ExecutionPeriod</code> instance by given title if it exists
     *
     * @param title
     * @return existing execution period or <code>null</code> if it's not found
     */
    public static ExecutionPeriod get(String title) {

        Optional<ExecutionPeriod> found = Arrays.stream(ExecutionPeriod.values())
                .filter(executionPeriod -> executionPeriod.title.equals(title))
                .findFirst();

        return found.orElse(null);
    }
}

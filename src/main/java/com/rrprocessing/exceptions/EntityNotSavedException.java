package com.rrprocessing.exceptions;

/**
 * <code>EntityNotSavedException</code> is a <code>RuntimeException</code>
 * subclass that can be thrown if entities were not saved when it is supposed to
 */
public class EntityNotSavedException extends RuntimeException{

    public EntityNotSavedException(String message) {

        super(message);
    }
}

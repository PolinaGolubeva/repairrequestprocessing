package com.rrprocessing.exceptions;

/**
 * <code>NotFoundException</code> is a <code>RuntimeException</code>
 * subclass that can be thrown if entities can not be found
 */
public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {

        super(message);
    }
}

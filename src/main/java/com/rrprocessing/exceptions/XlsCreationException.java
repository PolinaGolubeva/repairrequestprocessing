package com.rrprocessing.exceptions;

/**
 * <code>XlsCreationException</code> is a <code>RuntimeException</code>
 * subclass that can be thrown if xlsx-file can not be created
 */
public class XlsCreationException extends RuntimeException {

    public XlsCreationException(String message) {

        super(message);
    }
}

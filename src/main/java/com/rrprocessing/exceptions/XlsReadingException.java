package com.rrprocessing.exceptions;

/**
 * <code>XlsReadingException</code> is a <code>RuntimeException</code>
 * subclass that can be thrown if errors were encountered while reading xlsx-file
 */
public class XlsReadingException extends RuntimeException {

    public XlsReadingException(String message) {
        super(message);
    }

    public XlsReadingException(String message, Throwable e) {
        super(message, e);
    }
}

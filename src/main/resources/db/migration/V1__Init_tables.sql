CREATE TABLE IF NOT EXISTS municipality (
    id SERIAL PRIMARY KEY,
    code INTEGER,
    title VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS work_type (
    id SERIAL PRIMARY KEY,
    title VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS additional_number (
    id SERIAL PRIMARY KEY,
    add_number VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS comment (
    id SERIAL PRIMARY KEY,
    text VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS execution_period (
    id SERIAL PRIMARY KEY,
    title VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS request_status (
    id SERIAL PRIMARY KEY,
    is_rejected BOOLEAN,
    comment_id INTEGER,
    included_cost NUMERIC(10,2),

    CONSTRAINT fk_request_status_comment
        FOREIGN KEY (comment_id)
        REFERENCES comment(id)
);

CREATE TABLE IF NOT EXISTS request_execution (
    id SERIAL PRIMARY KEY,
    execution_period_id INTEGER,
    cost NUMERIC(10,2),
    request_status_id INTEGER,

    CONSTRAINT fk_request_execution_execution_period
        FOREIGN KEY (execution_period_id)
        REFERENCES execution_period(id),
    CONSTRAINT fk_request_execution_request_status
        FOREIGN KEY (request_status_id)
        REFERENCES request_status
);

CREATE TABLE IF NOT EXISTS request (
    id SERIAL PRIMARY KEY,
    municipality_id INTEGER,
    request_year INTEGER,
    object_name VARCHAR(255),
    work_type_id INTEGER,
    additional_number_id INTEGER,
    area NUMERIC(10, 2),
    declared_cost NUMERIC (10,2),
    request_status_id INTEGER,
    is_request_repeated BOOLEAN,

    CONSTRAINT fk_request_municipality
        FOREIGN KEY (municipality_id)
        REFERENCES municipality(id),
    CONSTRAINT fk_request_work_type
        FOREIGN KEY (work_type_id)
        REFERENCES work_type(id),
    CONSTRAINT fk_request_additional_number
        FOREIGN KEY (additional_number_id)
        REFERENCES additional_number(id),
    CONSTRAINT fk_request_request_status
        FOREIGN KEY (request_status_id)
        REFERENCES request_status(id)
);

CREATE TABLE IF NOT EXISTS cadastral_number (
    id SERIAL PRIMARY KEY,
    cad_number VARCHAR(255) UNIQUE
);

CREATE TABLE IF NOT EXISTS request_cadastral_number (
    id SERIAL PRIMARY KEY,
    request_id INTEGER,
    cadastral_id INTEGER,

    CONSTRAINT fk_request_cadastral_number_request
        FOREIGN KEY (request_id)
        REFERENCES request(id),
    CONSTRAINT fk_request_cadastral_number_cadastral_number
        FOREIGN KEY (cadastral_id)
        REFERENCES cadastral_number(id)
);

INSERT INTO work_type (id, title) VALUES (1, 'Ремонт');
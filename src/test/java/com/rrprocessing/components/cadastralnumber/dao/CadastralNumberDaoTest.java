package com.rrprocessing.components.cadastralnumber.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import java.util.List;
import java.util.Optional;

@ActiveProfiles("test")
@SpringBootTest
@Sql(value = {"classpath:/scripts/cadastralnumber/cadastral-number-test-schema.sql",
        "classpath:/scripts/cadastralnumber/cadastral-number-test-data.sql"},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:/scripts/cadastralnumber/cadastral-number-test-clean-data.sql",
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class CadastralNumberDaoTest {

    @Autowired
    private CadastralNumberDao dao;

    private CadastralNumber cadastralNumber1;
    private CadastralNumber cadastralNumber2;
    private CadastralNumber cadastralNumber3;
    private CadastralNumber cadastralNumber4;

    @BeforeEach
    void prepareData() {

        cadastralNumber1 = new CadastralNumber();
        cadastralNumber1.setId(1L);
        cadastralNumber1.setNumber("cadastral 1");

        cadastralNumber2 = new CadastralNumber();
        cadastralNumber2.setId(2L);
        cadastralNumber2.setNumber("cadastral 2");

        cadastralNumber3 = new CadastralNumber();
        cadastralNumber3.setNumber("cadastral 3");

        cadastralNumber4 = new CadastralNumber();
        cadastralNumber4.setNumber("cadastral 4");
    }

    @Test
    void find_cadastralNumberExists_cadastralNumberIsReturned() {

        // WHEN
        Optional<CadastralNumber> actual1 = dao.find(cadastralNumber1.getNumber());
        Optional<CadastralNumber> actual2 = dao .find(cadastralNumber2.getNumber());

        // THEN
        assertThat(actual1).isNotEmpty().contains(cadastralNumber1);
        assertThat(actual2).isNotEmpty().contains(cadastralNumber2);
    }

    @Test
    void find_cadastralNumberDoesNotExist_emptyOptionalIsReturned() {

        // WHEN
        Optional<CadastralNumber> actual = dao.find("cadastral number");

        // THEN
        assertThat(actual).isEmpty();
    }

    @Test
    void find_cadastralNumbersAreFound_cadastralNumbersAreReturned() {

        // GIVEN
        List<String> numbers = List.of(cadastralNumber1.getNumber(), cadastralNumber2.getNumber());

        // WHEN
        List<CadastralNumber> actual = dao.find(numbers);

        // THEN
        assertThat(actual).containsExactly(cadastralNumber1, cadastralNumber2);
    }

    @Test
    void find_cadastralNumbersAreNotFound_emptyListIsReturned() {

        // GIVEN
        List<String> numbers = List.of("cadastral number");

        // WHEN
        List<CadastralNumber> actual = dao.find(numbers);

        // THEN
        assertThat(actual).isEmpty();
    }

    @Test
    void find_oneOfCadastralNumbersIsFound_listWithOneCadastralNumberIsReturned() {

        // GIVEN
        List<String> numbers = List.of(cadastralNumber1.getNumber(), cadastralNumber3.getNumber());

        // WHEN
        List<CadastralNumber> actual = dao.find(numbers);

        // THEN
        assertThat(actual).containsExactly(cadastralNumber1);
    }

    @Test
    void save_newCadastralNumberIsGiven_cadastralNumberIsSavedAndIdIsReturned() {

        // WHEN
        Long id = dao.save(cadastralNumber3);

        // THEN
        assertThat(id).isNotNull();
    }

    @Test
    void save_existingNumberIsGiven_cadastralNumberIsNotSavedAndNullIsReturned() {

        // WHEN
        Long id = dao.save(cadastralNumber1);

        // THEN
        assertThat(id).isNull();
    }

    @Test
    void save_listOfCadastralNumersIsGiven_cadastralNumbersAreSaved() {

        // WHEN
        Throwable throwable = catchThrowable(() -> dao.save(List.of(cadastralNumber1, cadastralNumber3, cadastralNumber4)));

        // THEN
        assertThat(throwable).isNull();
    }
}

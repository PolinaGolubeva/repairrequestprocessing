package com.rrprocessing.components.cadastralnumber.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import com.rrprocessing.components.cadastralnumber.dao.CadastralNumberDao;
import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.exceptions.EntityNotSavedException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class CadastralNumberServiceImplTest {

    @Spy
    @InjectMocks
    private CadastralNumberServiceImpl cadastralNumberService;

    @Mock
    private CadastralNumberDao cadastralNumberDao;

    @Mock
    private CadastralNumber cadastralNumber1;

    @Mock
    private CadastralNumber cadastralNumber2;

    private static final String CADASTRAL_NUMBER = "cadastral number";
    private static final Long ID = 123L;

    @Test
    void save_newCadastralNumberIsGiven_cadastralNumberIsSavedAndReturned() {

        // GIVEN
        Mockito.when(cadastralNumber1.getNumber()).thenReturn(CADASTRAL_NUMBER);
        Mockito.when(cadastralNumberDao.find(Mockito.anyString())).thenReturn(Optional.empty());
        Mockito.when(cadastralNumberDao.save(Mockito.any(CadastralNumber.class))).thenReturn(ID);

        // WHEN
        CadastralNumber actual = cadastralNumberService.save(cadastralNumber1);

        // THEN
        Mockito.verify(cadastralNumberDao).find(CADASTRAL_NUMBER);
        Mockito.verify(cadastralNumberDao).save(cadastralNumber1);
        Mockito.verify(cadastralNumber1).setId(ID);
        assertThat(actual).isEqualTo(cadastralNumber1);
    }

    @Test
    void save_newCadastralNumberIsNotSaved_entityNotSavedExceptionIsThrown() {

        // GIVEN
        Mockito.when(cadastralNumber1.getNumber()).thenReturn(CADASTRAL_NUMBER);
        Mockito.when(cadastralNumberDao.find(Mockito.anyString())).thenReturn(Optional.empty());
        Mockito.when(cadastralNumberDao.save(Mockito.any(CadastralNumber.class))).thenReturn(null);

        // WHEN
        Throwable throwable = catchThrowable(() -> cadastralNumberService.save(cadastralNumber1));

        // THEN
        Mockito.verify(cadastralNumberDao).find(CADASTRAL_NUMBER);
        Mockito.verify(cadastralNumberDao).save(cadastralNumber1);
        assertThat(throwable).isInstanceOf(EntityNotSavedException.class);

    }

    @Test
    void save_existingCadastralNumberIsGiven_existingCadastralNumberIsReturned() {

        // GIVEN
        Mockito.when(cadastralNumber1.getNumber()).thenReturn(CADASTRAL_NUMBER);
        Mockito.when(cadastralNumberDao.find(Mockito.anyString())).thenReturn(Optional.of(cadastralNumber2));

        // WHEN
        CadastralNumber actual = cadastralNumberService.save(cadastralNumber1);

        // THEN
        Mockito.verify(cadastralNumberDao).find(CADASTRAL_NUMBER);
        Mockito.verify(cadastralNumberDao, Mockito.never()).save(Mockito.any(CadastralNumber.class));
        assertThat(actual).isEqualTo(cadastralNumber2);
    }

    @Test
    void save_listOfCadastralNumbersIsGiven_cadastralNumbersAreSavedAndReturned() {

        // GIVEN
        String number1 = "cadastral1";
        Mockito.when(cadastralNumber1.getNumber()).thenReturn(number1);
        String number2 = "cadastral2";
        Mockito.when(cadastralNumber2.getNumber()).thenReturn(number2);
        List<CadastralNumber> cadastralNumbers = List.of(cadastralNumber1, cadastralNumber2);
        Mockito.when(cadastralNumberDao.find(Mockito.anyList())).thenReturn(cadastralNumbers);

        // WHEN
        List<CadastralNumber> actual = cadastralNumberService.save(cadastralNumbers);

        // THEN
        Mockito.verify(cadastralNumberDao).save(cadastralNumbers);
        Mockito.verify(cadastralNumberDao).find(List.of(number1, number2));
        assertThat(actual).containsExactly(cadastralNumber1, cadastralNumber2);
    }

    @Test
    void save_emptyListIsGiven_emptyListIsReturned() {

        // WHEN
        List<CadastralNumber> actual = cadastralNumberService.save(Collections.emptyList());

        // THEN
        Mockito.verifyNoInteractions(cadastralNumberDao);
        assertThat(actual).isEmpty();
    }

    @Test
    void save_oneCadastralNumberIsNotSaved_entityNotSavedExceptionIsThrown() {

        // GIVEN
        String number1 = "cadastral1";
        Mockito.when(cadastralNumber1.getNumber()).thenReturn(number1);
        String number2 = "cadastral2";
        Mockito.when(cadastralNumber2.getNumber()).thenReturn(number2);
        List<CadastralNumber> cadastralNumbers = List.of(cadastralNumber1, cadastralNumber2);
        Mockito.when(cadastralNumberDao.find(Mockito.anyList())).thenReturn(List.of(cadastralNumber1));

        // WHEN
       Throwable throwable = catchThrowable(() -> cadastralNumberService.save(cadastralNumbers));

        // THEN
        Mockito.verify(cadastralNumberDao).save(cadastralNumbers);
        Mockito.verify(cadastralNumberDao).find(List.of(number1, number2));
        assertThat(throwable).isInstanceOf(EntityNotSavedException.class);
    }
}

package com.rrprocessing.components.export.controller;

import static org.assertj.core.api.Assertions.assertThat;

import com.rrprocessing.components.export.service.XlsExportService;
import com.rrprocessing.exceptions.NotFoundException;
import com.rrprocessing.exceptions.XlsCreationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(XlsController.class)
class XlsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private XlsExportService exportService;

    private static final Integer YEAR = 2020;
    private static final Integer MUNICIPALITY_CODE = 123;
    private static final String URI = String.format("/api/xlsx/year/%s/municipality/%s", YEAR, MUNICIPALITY_CODE);
    private static final String ERROR_MESSAGE = "Error message";
    private static final byte[] CONTENT = "some content".getBytes();

    @Test
    void getByYearAndMunicipalityCode_listOfRequestsIsFound_listIsSent() throws Exception {

        // GIVEN
        Mockito.when(exportService.getRequestsExcel(Mockito.anyInt(), Mockito.anyInt())).thenReturn(CONTENT);

        // WHEN
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(URI))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        byte[] actual = result.getResponse().getContentAsByteArray();
        String contentType = result.getResponse().getContentType();

        // THEN
        Mockito.verify(exportService).getRequestsExcel(YEAR, MUNICIPALITY_CODE);
        assertThat(actual).isEqualTo(CONTENT);
        assertThat(contentType).isEqualTo("application/force-download");
    }

    @Test
    void getByYearAndMunicipalityCode_notFoundExceptionIsThrown_responseWithNotFoundStatusIsReturned() throws Exception {

        // GIVEN
        Mockito.when(exportService.getRequestsExcel(Mockito.anyInt(), Mockito.anyInt()))
                .thenThrow(new NotFoundException(ERROR_MESSAGE));

        // WHEN
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(URI))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andReturn();
        String actual = result.getResponse().getContentAsString();

        // THEN
        Mockito.verify(exportService).getRequestsExcel(YEAR, MUNICIPALITY_CODE);
        assertThat(actual).isEqualTo(ERROR_MESSAGE);
    }

    @Test
    void getByYearAndMunicipalityCode_xlsCreationExceptionIsThrown_responseWithServiceUnavailableStatusIsReturned() throws Exception {

        // GIVEN
        Mockito.when(exportService.getRequestsExcel(Mockito.anyInt(), Mockito.anyInt()))
                .thenThrow(new XlsCreationException(ERROR_MESSAGE));

        // WHEN
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(URI))
                .andExpect(MockMvcResultMatchers.status().isServiceUnavailable())
                .andReturn();
        String actual = result.getResponse().getContentAsString();

        // THEN
        Mockito.verify(exportService).getRequestsExcel(YEAR, MUNICIPALITY_CODE);
        assertThat(actual).isEqualTo(ERROR_MESSAGE);
    }

    @Test
    void getByYearAndMunicipalityCode_runtimeExceptionIsThrown_responseWithInternalServerErrorStatusIsReturned() throws Exception {

        // GIVEN
        Mockito.when(exportService.getRequestsExcel(Mockito.anyInt(), Mockito.anyInt()))
                .thenThrow(new RuntimeException(ERROR_MESSAGE));

        // WHEN
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(URI))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andReturn();
        String actual = result.getResponse().getContentAsString();

        // THEN
        Mockito.verify(exportService).getRequestsExcel(YEAR, MUNICIPALITY_CODE);
        assertThat(actual).isEqualTo(ERROR_MESSAGE);
    }
}

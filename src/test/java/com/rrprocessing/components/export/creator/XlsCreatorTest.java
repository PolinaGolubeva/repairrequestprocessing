package com.rrprocessing.components.export.creator;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.components.request.domain.RepairRequest;
import org.apache.poi.ss.usermodel.Cell;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class XlsCreatorTest {

    @Spy
    private XlsCreator creator;

    @Mock
    private Cell cell;

    @Mock
    private RepairRequest request;

    @Test
    void fillNumberCell_numberIsGiven_numberIsSet() {

        // GIVEN
        int index = 123;
        Mockito.doNothing().when(cell).setCellValue(Mockito.anyDouble());

        // WHEN
        creator.fillNumberCell(cell, index);

        // THEN
        Mockito.verify(cell).setCellValue(index);
    }

    @Test
    void fillObjectNameCell_repairRequestIsGiven_objectNameIsSet() {

        // GIVEN
        String objectName = "object name";
        Mockito.when(request.getObjectName()).thenReturn(objectName);
        Mockito.doNothing().when(cell).setCellValue(Mockito.anyString());

        // WHEN
        creator.fillObjectNameCell(cell, request);

        // THEN
        Mockito.verify(cell).setCellValue(objectName);
    }

    @Test
    void fillAddressCell_repairRequestIsGiven_addressIsSet() {

        // GIVEN
        String address = "address";
        Mockito.when(request.getAddress()).thenReturn(address);
        Mockito.doNothing().when(cell).setCellValue(Mockito.anyString());

        // WHEN
        creator.fillAddressCell(cell, request);

        // THEN
        Mockito.verify(cell).setCellValue(address);
    }

    @Test
    void fillCadastralNumberCell_requestWithListOfCadastralNumbersIsGiven_cadastralNumbersAreSet() {

        // GIVEN
        CadastralNumber cadastralNumber1 = new CadastralNumber();
        cadastralNumber1.setNumber("cadastral1");
        CadastralNumber cadastralNumber2 = new CadastralNumber();
        cadastralNumber2.setNumber("cadastral2");

        Mockito.when(request.getCadastralNumbers()).thenReturn(List.of(cadastralNumber1, cadastralNumber2));
        Mockito.doNothing().when(cell).setCellValue(Mockito.anyString());

        // WHEN
        creator.fillCadastralNumberCell(cell, request);

        // THEN
        String expected = "cadastral1\ncadastral2";
        Mockito.verify(cell).setCellValue(expected);
    }

    @Test
    void fillCadastralNumberCell_requestWithEmptyListOfCadastralNumbersIsGiven_emptyCadastralNumbersAreSet() {

        // GIVEN
        Mockito.when(request.getCadastralNumbers()).thenReturn(Collections.emptyList());
        Mockito.doNothing().when(cell).setCellValue(Mockito.anyString());

        // WHEN
        creator.fillCadastralNumberCell(cell, request);

        // THEN
        Mockito.verify(cell).setCellValue("");
    }

    @Test
    void fillAnotherNumberCell_requestIsGiven_additionalNumberIsSet() {

        // GIVEN
        String additionalNumber = "additional number";
        Mockito.when(request.getAdditionalNumber()).thenReturn(additionalNumber);
        Mockito.doNothing().when(cell).setCellValue(Mockito.anyString());

        // WHEN
        creator.fillAnotherNumberCell(cell, request);

        // THEN
        Mockito.verify(cell).setCellValue(additionalNumber);
    }

    @Test
    void fillAreaCell_requestIsGiven_areaIsSet() {

        // GIVEN
        double area = 111.11;
        Mockito.when(request.getArea()).thenReturn(area);
        Mockito.doNothing().when(cell).setCellValue(Mockito.anyDouble());

        // WHEN
        creator.fillAreaCell(cell, request);

        // THEN
        Mockito.verify(cell).setCellValue(area);
    }

    @Test
    void fillWorkTypeCell_requestIsGiven_workTypeIsSet() {

        // GIVEN
        String workType = "work type";
        Mockito.when(request.getWorkType()).thenReturn(workType);
        Mockito.doNothing().when(cell).setCellValue(Mockito.anyString());

        // WHEN
        creator.fillWorkTypeCell(cell, request);

        // THEN
        Mockito.verify(cell).setCellValue(workType);
    }

    @Test
    void fillPlannedCostCell_requestIsGiven_plannedCostIsSet() {

        // GIVEN
        double cost = 222.22;
        Mockito.when(request.getDeclaredCost()).thenReturn(cost);
        Mockito.doNothing().when(cell).setCellValue(Mockito.anyDouble());

        // WHEN
        creator.fillPlannedCostCell(cell, request);

        // THEN
        Mockito.verify(cell).setCellValue(cost);
    }
}

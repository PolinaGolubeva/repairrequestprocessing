package com.rrprocessing.components.export.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import com.rrprocessing.components.export.creator.XlsCreator;
import com.rrprocessing.components.request.domain.RepairRequest;
import com.rrprocessing.components.request.service.RequestService;
import com.rrprocessing.exceptions.XlsCreationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import java.io.IOException;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class XlsExportServiceImplTest {

    @Spy
    @InjectMocks
    private XlsExportServiceImpl xlsExportService;

    @Mock
    private RequestService requestService;

    @Mock
    private XlsCreator xlsCreator;

    private RepairRequest request1;
    private RepairRequest request2;

    private static final String XLS_CONTENT = "some content";
    private static final Integer YEAR = 2020;
    private static final Integer CODE = 123;

    @BeforeEach
    void setUp() {

        request1 = new RepairRequest();
        request2 = new RepairRequest();
        Mockito.when(requestService.find(Mockito.anyInt(), Mockito.anyInt())).thenReturn(List.of(request1, request2));
    }

    @Test
    void getRequestsExcel_requestsAreFoundAndFileIsCreated_byteArrayOfFileIsReturned() throws IOException {

        // GIVEN
        Mockito.when(xlsCreator.getRequestXls(Mockito.anyList())).thenReturn(XLS_CONTENT.getBytes());

        // WHEN
        byte[] actual = xlsExportService.getRequestsExcel(YEAR, CODE);

        // THEN
        Mockito.verify(requestService).find(YEAR, CODE);
        Mockito.verify(xlsCreator).getRequestXls(List.of(request1, request2));
        assertThat(actual).isEqualTo(XLS_CONTENT.getBytes());
    }

    @Test
    void getRequestsExcel_creatorThrewIOException_xlsCreationExceptionIsThrown() throws IOException {

        // GIVEN
        Mockito.when(xlsCreator.getRequestXls(Mockito.anyList())).thenThrow(IOException.class);

        // WHEN
        Throwable throwable = catchThrowable(() -> xlsExportService.getRequestsExcel(YEAR, CODE));

        // THEN
        Mockito.verify(requestService).find(YEAR, CODE);
        Mockito.verify(xlsCreator).getRequestXls(List.of(request1, request2));
        assertThat(throwable).isInstanceOf(XlsCreationException.class);
    }
}

package com.rrprocessing.components.municipality.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import com.rrprocessing.components.municipality.domain.Municipality;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import java.util.List;
import java.util.Optional;

@ActiveProfiles("test")
@SpringBootTest
@Sql(value = {"classpath:/scripts/municipality/municipality-test-schema.sql",
        "classpath:/scripts/municipality/municipality-test-data.sql"},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:/scripts/municipality/municipality-test-clean-data.sql",
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class MunicipalityDaoTest {

    @Autowired
    private MunicipalityDao dao;

    private Municipality municipality1;
    private Municipality municipality2;
    private Municipality municipality3;
    private Municipality municipality4;

    @BeforeEach
    void prepareData() {

        municipality1 = new Municipality();
        municipality1.setId(1L);
        municipality1.setCode(111);
        municipality1.setTitle("municipality 111");

        municipality2 = new Municipality();
        municipality2.setId(2L);
        municipality2.setCode(222);
        municipality2.setTitle("municipality 222");

        municipality3 = new Municipality();
        municipality3.setCode(333);
        municipality3.setTitle("municipality 333");

        municipality4 = new Municipality();
        municipality4.setCode(444);
        municipality4.setTitle("municipality 444");
    }

    @Test
    void find_municipalityExists_municipalityIsReturned() {

        // WHEN
        Optional<Municipality> actual1 = dao.find(111);
        Optional<Municipality> actual2 = dao.find(222);

        // THEN
        assertThat(actual1).isNotEmpty().contains(municipality1);
        assertThat(actual2).isNotEmpty().contains(municipality2);
    }

    @Test
    void find_municipalityDoesNotExist_emptyOptionalIsReturned() {

        // WHEN
        Optional<Municipality> actual = dao.find(333);

        // THEN
        assertThat(actual).isEmpty();
    }

    @Test
    void find_municipalitiesExist_listOfMunicipalitiesIsReturned() {

        // GIVEN
        List<Integer> codes = List.of(111, 222);

        // WHEN
        List<Municipality> actual = dao.find(codes);

        // THEN
        assertThat(actual).containsExactly(municipality1, municipality2);
    }

    @Test
    void save_newMunicipalityIsGiven_municipalityIsSavedAndIdIsReturned() {

        // WHEN
        Long id = dao.save(municipality3);

        // THEN
        assertThat(id).isNotNull();
    }

    @Test
    void save_municipalityExistsInDatabase_municipalityIsNotSavedAndNullIsReturned() {

        // WHEN
        Long id = dao.save(municipality2);

        // THEN
        assertThat(id).isNull();
    }

    @Test
    void save_municipalitiesAreGiven_municipalitiesAreSaved() {

        // GIVEN
        List<Municipality> municipalities = List.of(municipality1, municipality3, municipality4);

        // WHEN
        Throwable throwable = catchThrowable(() -> dao.save(municipalities));

        // THEN
        assertThat(throwable).isNull();
    }
}

package com.rrprocessing.components.municipality.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import com.rrprocessing.components.municipality.dao.MunicipalityDao;
import com.rrprocessing.components.municipality.domain.Municipality;
import com.rrprocessing.exceptions.EntityNotSavedException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class MunicipalityServiceImplTest {

    @Spy
    @InjectMocks
    private MunicipalityServiceImpl municipalityService;

    @Mock
    private MunicipalityDao municipalityDao;

    @Mock
    private Municipality municipality1;

    @Mock
    private Municipality municipality2;

    private static final Integer CODE = 123;
    private static final Long ID = 345L;

    @Test
    void save_newMunicipalityIsGiven_municipalityIsSaved() {

        // GIVEN
        Mockito.when(municipality1.getCode()).thenReturn(CODE);
        Mockito.when(municipalityDao.find(Mockito.anyInt())).thenReturn(Optional.empty());
        Mockito.when(municipalityDao.save(municipality1)).thenReturn(ID);

        // WHEN
        Municipality actual = municipalityService.save(municipality1);

        // THEN
        Mockito.verify(municipalityDao).find(CODE);
        Mockito.verify(municipalityDao).save(municipality1);
        Mockito.verify(municipality1).setId(ID);
        assertThat(actual).isEqualTo(municipality1);
    }

    @Test
    void save_municipalityExistsInDatabase_existingMunicipalityIsReturned() {

        // GIVEN
        Mockito.when(municipality1.getCode()).thenReturn(CODE);
        Municipality existing = Mockito.mock(Municipality.class);
        Mockito.when(municipalityDao.find(Mockito.anyInt())).thenReturn(Optional.of(existing));

        // WHEN
        Municipality actual = municipalityService.save(municipality1);

        // THEN
        Mockito.verify(municipalityDao, Mockito.never()).save(municipality1);
        Mockito.verify(municipalityDao).find(CODE);
        assertThat(actual).isEqualTo(existing);
    }

    @Test
    void save_municipalitiesAreGiven_municipalitiesAreSavedAndReturned() {

        // GIVEN
        List<Municipality> municipalities = List.of(municipality1, municipality2);
        Integer code1 = 111;
        Mockito.when(municipality1.getCode()).thenReturn(code1);
        Integer code2 = 222;
        Mockito.when(municipality2.getCode()).thenReturn(code2);
        Mockito.when(municipalityDao.find(List.of(code1, code2))).thenReturn(municipalities);

        // WHEN
        List<Municipality> actual = municipalityService.save(municipalities);

        // THEN
        Mockito.verify(municipalityDao).save(municipalities);
        assertThat(actual).containsExactly(municipality1, municipality2);
    }

    @Test
    void save_emptyListIsGiven_emptyListIsReturned() {

        // WHEN
        List<Municipality> actual = municipalityService.save(Collections.emptyList());

        // THEN
        Mockito.verifyNoInteractions(municipalityDao);
        assertThat(actual).isEmpty();
    }

    @Test
    void save_municipalitiesAreNotSaved_entityNotSavedExceptionIsThrown() {

        // GIVEN
        List<Municipality> municipalities = List.of(municipality1, municipality2);
        Mockito.when(municipality1.getCode()).thenReturn(111);
        Mockito.when(municipality2.getCode()).thenReturn(222);
        Mockito.when(municipalityDao.find(List.of(111, 222))).thenReturn(List.of(municipality1));

        // WHEN
        Throwable throwable = catchThrowable(() -> municipalityService.save(municipalities));

        // THEN
        Mockito.verify(municipalityDao).save(municipalities);
        assertThat(throwable).isInstanceOf(EntityNotSavedException.class);
    }

    @Test
    void find_municipalityIsFound_municipalityIsReturned() {

        // GIVEN
        Mockito.when(municipalityDao.find(Mockito.anyInt())).thenReturn(Optional.of(municipality1));

        // WHEN
        Optional<Municipality> actual = municipalityService.find(CODE);

        // THEN
        Mockito.verify(municipalityDao).find(CODE);
        assertThat(actual).isNotEmpty().contains(municipality1);
    }

    @Test
    void find_municipalityIsNotFound_emptyOptionalIsReturned() {

        // GIVEN
        Mockito.when(municipalityDao.find(Mockito.anyInt())).thenReturn(Optional.empty());

        // WHEN
        Optional<Municipality> actual = municipalityService.find(CODE);

        // THEN
        Mockito.verify(municipalityDao).find(CODE);
        assertThat(actual).isEmpty();
    }

    @Test
    void find_municipalitiesAreFound_municipalitiesAreReturned() {

        // GIVEN
        List<Integer> codes = List.of(111, 222);
        Mockito.when(municipalityDao.find(Mockito.anyList())).thenReturn(List.of(municipality1, municipality2));

        // WHEN
        List<Municipality> actual = municipalityService.find(codes);

        // THEN
        Mockito.verify(municipalityDao).find(codes);
        assertThat(actual).containsExactly(municipality1, municipality2);
    }
}

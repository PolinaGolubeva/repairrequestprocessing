package com.rrprocessing.components.repairrequest.controller;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rrprocessing.components.municipality.domain.Municipality;
import com.rrprocessing.components.request.controller.RequestController;
import com.rrprocessing.components.request.domain.RepairRequest;
import com.rrprocessing.components.request.service.RequestService;
import com.rrprocessing.exceptions.NotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(RequestController.class)
class RequestControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RequestService requestService;

    private static final Integer YEAR = 2020;
    private static final Integer MUNICIPALITY_CODE = 123;
    private static final String URI = String.format("/api/year/%s/municipality/%s", YEAR, MUNICIPALITY_CODE);
    private static final String ERROR_MESSAGE = "Error message";

    @Test
    void getByYearAndMunicipalityCode_listOfRequestsIsFound_listIsSent() throws Exception {

        // GIVEN
        Municipality municipality = new Municipality();
        municipality.setCode(MUNICIPALITY_CODE);

        RepairRequest request1 = new RepairRequest();
        request1.setYear(YEAR);
        request1.setMunicipality(municipality);

        RepairRequest request2 = new RepairRequest();
        request2.setYear(YEAR);
        request2.setMunicipality(municipality);

        List<RepairRequest> requests = List.of(request1, request2);

        Mockito.when(requestService.find(Mockito.anyInt(), Mockito.anyInt())).thenReturn(requests);

        // WHEN
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(URI))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        List<RepairRequest> actual = objectMapper.readValue(content, new TypeReference<>(){});

        // THEN
        Mockito.verify(requestService).find(YEAR, MUNICIPALITY_CODE);
        assertThat(actual).containsAll(requests);
    }

    @Test
    void getByYearAndMunicipalityCode_requestsAreNotFound_emptyJsonIsSent() throws Exception {

        // GIVEN
        Mockito.when(requestService.find(Mockito.anyInt(), Mockito.anyInt())).thenReturn(Collections.emptyList());

        // WHEN
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(URI))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        String actual = result.getResponse().getContentAsString();

        // THEN
        Mockito.verify(requestService).find(YEAR, MUNICIPALITY_CODE);
        assertThat(actual).isEqualTo("[]");
    }

    @Test
    void getByYearAndMunicipalityCode_notFoundExceptionIsThrown_responseWithNotFoundStatusIsReturned() throws Exception {

        // GIVEN
        Mockito.when(requestService.find(Mockito.anyInt(), Mockito.anyInt())).thenThrow(new NotFoundException(ERROR_MESSAGE));

        // WHEN
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(URI))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andReturn();
        String actual = result.getResponse().getContentAsString();

        // THEN
        Mockito.verify(requestService).find(YEAR, MUNICIPALITY_CODE);
        assertThat(actual).isEqualTo(ERROR_MESSAGE);
    }

    @Test
    void getByYearAndMunicipalityCode_runtimeExceptionIsThrown_responseWithInternalServerErrorStatusIsReturned() throws Exception {

        // GIVEN
        Mockito.when(requestService.find(Mockito.anyInt(), Mockito.anyInt())).thenThrow(new RuntimeException(ERROR_MESSAGE));

        // WHEN
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(URI))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andReturn();
        String actual = result.getResponse().getContentAsString();

        // THEN
        Mockito.verify(requestService).find(YEAR, MUNICIPALITY_CODE);
        assertThat(actual).isEqualTo(ERROR_MESSAGE);
    }
}

package com.rrprocessing.components.repairrequest.dao;

import static org.assertj.core.api.Assertions.assertThat;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.components.municipality.domain.Municipality;
import com.rrprocessing.components.request.dao.RequestDao;
import com.rrprocessing.components.request.domain.RepairRequest;
import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import java.util.Collections;
import java.util.List;

@ActiveProfiles("test")
@SpringBootTest
@Sql(value = {"classpath:/scripts/request/request-test-schema.sql",
        "classpath:/scripts/request/request-test-data.sql"},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:/scripts/request/request-test-clean-data.sql",
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class RequestDaoTest {

    @Autowired
    private RequestDao dao;

    private List<RepairRequest> requests1;
    private List<RepairRequest> requests2;
    private List<RepairRequest> requests3;

    private CadastralNumber cadastralNumber1;
    private CadastralNumber cadastralNumber2;
    private Municipality municipality1;

    @BeforeEach
    void prepareTestData() {

        municipality1 = new Municipality();
        municipality1.setId(11L);
        municipality1.setCode(111);
        municipality1.setTitle("municipality 111");

        Municipality municipality2 = new Municipality();
        municipality2.setId(12L);
        municipality2.setCode(222);
        municipality2.setTitle("municipality 222");

        cadastralNumber1 = new CadastralNumber();
        cadastralNumber1.setId(41L);
        cadastralNumber1.setNumber("cadastral number 1");

        cadastralNumber2 = new CadastralNumber();
        cadastralNumber2.setId(42L);
        cadastralNumber2.setNumber("cadastral number 2");

        RepairRequest request1 = new RepairRequest();
        request1.setId(51L);
        request1.setMunicipality(municipality1);
        request1.setYear(2017);
        request1.setObjectName("object 1");
        request1.setAddress("address 1");
        request1.setWorkType("work type");
        request1.setAdditionalNumber("additional number");
        request1.setArea(111.00);
        request1.setDeclaredCost(1111.00);
        request1.setIsRequestRepeated(false);
        request1.setCadastralNumbers(List.of(cadastralNumber1));

        RepairRequest request2 = new RepairRequest();
        request2.setId(52L);
        request2.setMunicipality(municipality2);
        request2.setYear(2017);
        request2.setObjectName("object 2");
        request2.setAddress("address 2");
        request2.setWorkType("work type");
        request2.setArea(222.00);
        request2.setDeclaredCost(2222.00);
        request2.setIsRequestRepeated(true);
        request2.setCadastralNumbers(List.of(cadastralNumber1, cadastralNumber2));

        RepairRequest request3 = new RepairRequest();
        request3.setId(53L);
        request3.setMunicipality(municipality2);
        request3.setYear(2017);
        request3.setObjectName("object 3");
        request3.setAddress("address 3");
        request3.setWorkType("work type");
        request3.setAdditionalNumber("another additional number");
        request3.setArea(333.00);
        request3.setDeclaredCost(3333.00);
        request3.setIsRequestRepeated(true);
        request3.setCadastralNumbers(List.of(cadastralNumber2));

        RepairRequest request4 = new RepairRequest();
        request4.setId(54L);
        request4.setMunicipality(municipality2);
        request4.setYear(2018);
        request4.setObjectName("object 4");
        request4.setAddress("address 4");
        request4.setWorkType("work type");
        request4.setArea(444.00);
        request4.setDeclaredCost(4444.00);
        request4.setIsRequestRepeated(false);
        request4.setCadastralNumbers(Collections.emptyList());

        requests1 = List.of(request1);
        requests2 = List.of(request2, request3);
        requests3 = List.of(request4);
    }

    @Test
    void find_requestsAreFound_listOfRequestsIsReturned() {

        // WHEN
        List<RepairRequest> actual1 = dao.findByYearAndMunicipality(2017, 111);
        List<RepairRequest> actual2 = dao.findByYearAndMunicipality(2017, 222);
        List<RepairRequest> actual3 = dao.findByYearAndMunicipality(2018, 222);

        // THEN
        assertThat(actual1).containsAll(requests1);
        assertThat(actual2).containsAll(requests2);
        assertThat(actual3).containsAll(requests3);
    }

    @Test
    void find_requestsAreNotFound_emptyListIsReturned() {

        // WHEN
        List<RepairRequest> actual = dao.findByYearAndMunicipality(2018, 111);

        // THEN
        assertThat(actual).isEmpty();
    }

    @Test
    void save_requestIsGiven_idOfSavedRequestIsReturned() {

        // GIVEN
        RepairRequest request = new RepairRequest();
        request.setMunicipality(municipality1);
        request.setCadastralNumbers(List.of(cadastralNumber1, cadastralNumber2));
        request.setAdditionalNumber("some additional number");
        request.setYear(2020);
        request.setObjectName("some object");
        request.setAddress("some address");
        request.setArea(12.0);
        request.setDeclaredCost(345.0);
        request.setWorkType("work type");
        request.setIsRequestRepeated(true);
        request.setRequestStatus(new RequestStatus());

        // WHEN
        Long id = dao.save(request);

        // THEN
        assertThat(id).isNotNull();
    }
}

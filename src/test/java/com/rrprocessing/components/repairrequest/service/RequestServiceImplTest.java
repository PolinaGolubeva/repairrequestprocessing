package com.rrprocessing.components.repairrequest.service;

import static org.assertj.core.api.Assertions.assertThat;

import com.rrprocessing.components.cadastralnumber.service.CadastralNumberService;
import com.rrprocessing.components.municipality.service.MunicipalityService;
import com.rrprocessing.components.request.service.RequestServiceImpl;
import com.rrprocessing.components.requeststatus.service.RequestStatusService;
import com.rrprocessing.components.request.dao.RequestDao;
import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.components.municipality.domain.Municipality;
import com.rrprocessing.components.request.domain.RepairRequest;
import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class RequestServiceImplTest {

    @Spy
    @InjectMocks
    private RequestServiceImpl requestService;

    @Mock
    private RequestDao requestDao;

    @Mock
    private MunicipalityService municipalityService;

    @Mock
    private CadastralNumberService cadastralNumberService;

    @Mock
    private RequestStatusService requestStatusService;

    private static final Integer YEAR = 2020;
    private static final Integer MUNICIPALITY_CODE = 123;

    @Test
    void find_requestsWithGivenYearAndCodeAreFound_requestsAreReturned() {

        // GIVEN
        RepairRequest request1 = Mockito.mock(RepairRequest.class);
        RepairRequest request2 = Mockito.mock(RepairRequest.class);
        Mockito.when(requestDao.findByYearAndMunicipality(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(List.of(request1, request2));

        // WHEN
        List<RepairRequest> actual = requestDao.findByYearAndMunicipality(YEAR, MUNICIPALITY_CODE);

        // THEN
        assertThat(actual).containsExactly(request1, request2);
        Mockito.verify(requestDao).findByYearAndMunicipality(YEAR, MUNICIPALITY_CODE);
    }

    @Test
    void find_requestsWithGivenYearAndCodeAreNotFound_emptyListIsReturned() {

        // GIVEN
        Mockito.when(requestDao.findByYearAndMunicipality(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(Collections.emptyList());

        // WHEN
        List<RepairRequest> actual = requestDao.findByYearAndMunicipality(YEAR, MUNICIPALITY_CODE);

        // THEN
        assertThat(actual).isEmpty();
        Mockito.verify(requestDao).findByYearAndMunicipality(YEAR, MUNICIPALITY_CODE);
    }

    @Test
    void save_requestIsGiven_allFieldsAndRequestAreSaved() {

        // GIVEN
        RepairRequest request = new RepairRequest();
        Municipality municipality1 = Mockito.mock(Municipality.class);
        Municipality municipality2 = Mockito.mock(Municipality.class);
        RequestStatus requestStatus1 = Mockito.mock(RequestStatus.class);
        RequestStatus requestStatus2 = Mockito.mock(RequestStatus.class);
        CadastralNumber cadastralNumber1 = Mockito.mock(CadastralNumber.class);
        CadastralNumber cadastralNumber2 = Mockito.mock(CadastralNumber.class);

        request.setMunicipality(municipality1);
        request.setCadastralNumbers(List.of(cadastralNumber1));
        request.setRequestStatus(requestStatus1);

        Mockito.when(municipalityService.save(Mockito.any(Municipality.class))).thenReturn(municipality2);
        Mockito.when(requestStatusService.save(Mockito.any(RequestStatus.class))).thenReturn(requestStatus2);
        Mockito.when(cadastralNumberService.save(Mockito.anyList())).thenReturn(List.of(cadastralNumber2));

        // WHEN
        requestService.save(request);

        // THEN
        Mockito.verify(municipalityService).save(municipality1);
        Mockito.verify(cadastralNumberService).save(List.of(cadastralNumber1));
        Mockito.verify(requestStatusService).save(requestStatus1);
        Mockito.verify(requestDao).save(request);
        assertThat(request).extracting(RepairRequest::getMunicipality, RepairRequest::getCadastralNumbers,
                        RepairRequest::getRequestStatus)
                .containsExactly(municipality2, List.of(cadastralNumber2), requestStatus2);
    }
}

package com.rrprocessing.components.requeststatus.dao;

import static org.assertj.core.api.Assertions.assertThat;

import com.rrprocessing.domain.ExecutionPeriod;
import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import java.util.Map;
import java.util.Optional;

@ActiveProfiles("test")
@SpringBootTest
@Sql(value = {"classpath:/scripts/requeststatus/request-status-test-schema.sql",
        "classpath:/scripts/requeststatus/request-status-test-data.sql"},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:/scripts/requeststatus/request-status-test-clean-data.sql",
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class RequestStatusDaoTest {

    @Autowired
    private RequestStatusDao dao;

    @Test
    void find_requestsAndRequestStatusesExist_requestStatusesAreReturned() {

        // WHEN
        Optional<RequestStatus> requestStatus1 = dao.find(41L);
        Optional<RequestStatus> requestStatus2 = dao.find(42L);
        Optional<RequestStatus> requestStatus3 = dao.find(43L);
        Optional<RequestStatus> requestStatus4 = dao.find(44L);

        // THEN
        assertThat(requestStatus1).isNotEmpty();
        assertThat(requestStatus1.get()).extracting(RequestStatus::getId, RequestStatus::getIsRejected,
                RequestStatus::getComment, RequestStatus::getIncludedCost)
                .containsExactly(21L, true, "some comment", 0.0);
        assertThat(requestStatus1.get().getExecutionCosts()).isEmpty();

        assertThat(requestStatus2).isNotEmpty();
        assertThat(requestStatus2.get()).extracting(RequestStatus::getId, RequestStatus::getIsRejected,
                        RequestStatus::getComment, RequestStatus::getIncludedCost)
                .containsExactly(22L, true, null, 0.0);
        assertThat(requestStatus2.get().getExecutionCosts()).isEmpty();

        assertThat(requestStatus3).isNotEmpty();
        assertThat(requestStatus3.get()).extracting(RequestStatus::getId, RequestStatus::getIsRejected,
                        RequestStatus::getComment, RequestStatus::getIncludedCost)
                .containsExactly(23L, false, null, 333.33);

        Map<ExecutionPeriod, Double> costMap = Map.of(ExecutionPeriod.FIRST_QUARTER, 11.11,
                ExecutionPeriod.SECOND_QUARTER, 22.22, ExecutionPeriod.THIRD_QUARTER, 33.33);
        assertThat(requestStatus3.get().getExecutionCosts()).containsAllEntriesOf(costMap);

        assertThat(requestStatus4).isNotEmpty();
        assertThat(requestStatus4.get()).extracting(RequestStatus::getId, RequestStatus::getIsRejected,
                        RequestStatus::getComment, RequestStatus::getIncludedCost)
                .containsExactly(24L, false, "other comment", 444.44);
        assertThat(requestStatus4.get().getExecutionCosts())
                .containsAllEntriesOf(Map.of(ExecutionPeriod.FOURTH_QUARTER, 44.44));
    }

    @Test
    void find_requestDoesNotExist_emptyOptionalIsReturned() {

        // WHEN
        Optional<RequestStatus> requestStatus = dao.find(123L);

        // THEN
        assertThat(requestStatus).isEmpty();
    }

    @Test
    void save_requestStatusIsGiven_requestStatusIsSavedAndReturnedWithId() {

        // GIVEN
        RequestStatus requestStatus = new RequestStatus();
        requestStatus.setComment("Comment");
        requestStatus.setIsRejected(false);
        requestStatus.setIncludedCost(123.45);
        requestStatus.setExecutionCosts(Map.of(ExecutionPeriod.SECOND_QUARTER, 12.34));

        // WHEN
        Long id = dao.save(requestStatus);

        // THEN
        assertThat(id).isNotNull();
    }
}

package com.rrprocessing.components.requeststatus.service;

import static org.assertj.core.api.Assertions.assertThat;

import com.rrprocessing.components.requeststatus.dao.RequestStatusDao;
import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class RequestStatusServiceImplTest {

    @Spy
    @InjectMocks
    private RequestStatusServiceImpl requestStatusService;

    @Mock
    private RequestStatusDao requestStatusDao;

    @Test
    void save_requestStatusIsGiven_requestStatusIsSavedAndReturned() {

        // GIVEN
        Long id = 123L;
        RequestStatus requestStatus = new RequestStatus();
        Mockito.when(requestStatusDao.save(Mockito.any(RequestStatus.class))).thenReturn(id);

        // WHEN
        RequestStatus actual = requestStatusService.save(requestStatus);

        // THEN
        Mockito.verify(requestStatusDao).save(requestStatus);
        assertThat(actual.getId()).isEqualTo(id);

        requestStatus.setId(id);
        assertThat(actual).isEqualTo(requestStatus);
    }

    @Test
    void save_requestStatusIsNull_statusIsNotSavedAndNullIsReturned() {

        // WHEN
        RequestStatus actual = requestStatusService.save(null);

        // THEN
        Mockito.verify(requestStatusDao, Mockito.never()).save(Mockito.any(RequestStatus.class));
        assertThat(actual).isNull();
    }

    @Test
    void find_requestStatusIsFound_requestStatusIsReturned() {

        // GIVEN
        Long requestId = 345L;
        RequestStatus requestStatus = new RequestStatus();
        Mockito.when(requestStatusDao.find(Mockito.anyLong())).thenReturn(Optional.of(requestStatus));

        // WHEN
        Optional<RequestStatus> actual = requestStatusService.find(requestId);

        // THEN
        Mockito.verify(requestStatusDao).find(requestId);
        assertThat(actual).isNotEmpty().contains(requestStatus);
    }

    @Test
    void find_requestStatusIsNotFound_emptyOptionalIsReturned() {

        // GIVEN
        Long requestId = 789L;
        Mockito.when(requestStatusDao.find(requestId)).thenReturn(Optional.empty());

        // WHEN
        Optional<RequestStatus> actual = requestStatusService.find(requestId);

        // THEN
        Mockito.verify(requestStatusDao).find(requestId);
        assertThat(actual).isEmpty();
    }
}

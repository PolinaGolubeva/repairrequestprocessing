package com.rrprocessing.components.xlsimport.converter;

import static org.assertj.core.api.Assertions.assertThat;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.components.municipality.domain.Municipality;
import com.rrprocessing.components.request.domain.RepairRequest;
import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import com.rrprocessing.components.xlsimport.dto.RowData;
import com.rrprocessing.domain.ExecutionPeriod;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@ExtendWith(MockitoExtension.class)
class RowDataToRepairRequestConverterTest {

    @Spy
    private RowDataToRepairRequestConverter converter;

    private RowData rowData;
    private RepairRequest request;

    @BeforeEach
    void setUp() {

        List<CadastralNumber> cadastralNumbers = new ArrayList<>();

        CadastralNumber cadastralNumber1 = new CadastralNumber();
        cadastralNumber1.setNumber("cadastral1");
        cadastralNumbers.add(cadastralNumber1);

        CadastralNumber cadastralNumber2 = new CadastralNumber();
        cadastralNumber2.setNumber("cadastral2");
        cadastralNumbers.add(cadastralNumber2);

        RequestStatus requestStatus = new RequestStatus();
        requestStatus.setIncludedCost(1111.11);
        requestStatus.setExecutionCosts(Map.of(ExecutionPeriod.FIRST_QUARTER, 11.11));
        requestStatus.setIsRejected(false);

        rowData = RowData.builder()
                .year(2018)
                .objectName("object name")
                .address("address")
                .cadastralNumbers("cadastral1\ncadastral2")
                .additionalNumber("additional")
                .area(111.11)
                .declaredCost(1111.11)
                .isRepeated(true)
                .municipalityCode(111)
                .municipalityTitle("municipality111")
                .includedCost(1111.11)
                .executionCosts(Map.of(ExecutionPeriod.FIRST_QUARTER, 11.11))
                .build();

        Municipality municipality = new Municipality();
        municipality.setCode(111);
        municipality.setTitle("municipality111");

        request = RepairRequest.builder()
                .year(2018)
                .objectName("object name")
                .address("address")
                .cadastralNumbers(cadastralNumbers)
                .additionalNumber("additional")
                .area(111.11)
                .declaredCost(1111.11)
                .isRequestRepeated(true)
                .municipality(municipality)
                .requestStatus(requestStatus)
                .workType("Ремонт")
                .build();
    }

    @Test
    void convert_rowDataWithAllFilledFieldsIsGiven_repairRequestIsReturned() {

        // WHEN
        RepairRequest actual = converter.convert(rowData);

        // THEN
        assertThat(actual).isEqualTo(request);
    }

    @Test
    void convert_rowDataWithoutCadastralNumberIsGiven_repairRequestWithEmptyCadastralNumberListIsReturned() {

        // GIVEN
        rowData.setCadastralNumbers(null);
        request.setCadastralNumbers(Collections.emptyList());

        // WHEN
        RepairRequest actual = converter.convert(rowData);

        // THEN
        assertThat(actual).isEqualTo(request);
    }

    @Test
    void convert_rowDataWithZeroIncludedCostIsGiven_repairRequestWithRejectedRequestStatusIsReturned() {

        // GIVEN
        rowData.setIncludedCost(0.0);
        rowData.setExecutionCosts(Collections.emptyMap());
        rowData.setRejectionReason("reject");

        RequestStatus requestStatus = new RequestStatus();
        requestStatus.setIsRejected(true);
        requestStatus.setIncludedCost(0.0);
        requestStatus.setExecutionCosts(Collections.emptyMap());
        requestStatus.setComment("reject");

        request.setRequestStatus(requestStatus);

        // WHEN
        RepairRequest actual = converter.convert(rowData);

        // THEN
        assertThat(actual).isEqualTo(request);
    }

    @Test
    void convert_rowDataWithNullIncludedCostIsGiven_repairRequestWithoutRequestStatusIsReturned() {

        // GIVEN
        rowData.setIncludedCost(null);
        rowData.setExecutionCosts(Collections.emptyMap());
        request.setRequestStatus(null);

        // WHEN
        RepairRequest actual = converter.convert(rowData);

        // THEN
        assertThat(actual).isEqualTo(request);
    }
}

package com.rrprocessing.components.xlsimport.reader;

import com.rrprocessing.components.xlsimport.ColumnNames;
import com.rrprocessing.components.xlsimport.dto.RowData;
import com.rrprocessing.domain.ExecutionPeriod;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ExtendWith(MockitoExtension.class)
class XlsReaderTest {

    @Spy
    @InjectMocks
    private XlsReader reader;

    @Mock
    private Cell cell;

    @Mock
    private RowData rowData;

    @Mock
    private Map<Integer, ColumnNames> columns;

    @Mock
    private CellRangeAddress mergedRegion;

    @Mock
    private FormulaEvaluator formulaEvaluator;

    @Mock
    private Sheet sheet;

    @BeforeEach
    void setUp() {

        ReflectionTestUtils.setField(reader, "columns", columns);
        ReflectionTestUtils.setField(reader, "mergedRegions", List.of(mergedRegion));
        ReflectionTestUtils.setField(reader, "formulaEvaluator", formulaEvaluator);
        ReflectionTestUtils.setField(reader, "sheet", sheet);
    }

    @Test
    void extractMunicipalityCode_cellWithNumericValueIsGiven_municipalityCodeIsExtracted() {

        // GIVEN
        int code = 123;
        Mockito.when(cell.getCellType()).thenReturn(CellType.NUMERIC);
        Mockito.when(cell.getNumericCellValue()).thenReturn((double) code);
        Mockito.doNothing().when(rowData).setMunicipalityCode(Mockito.anyInt());

        // WHEN
        reader.extractMunicipalityCode(cell, rowData);

        // THEN
        Mockito.verify(rowData).setMunicipalityCode(code);
    }

    @Test
    void extractMunicipalityCode_cellWithStringValueIsGiven_municipalityCodeIsExtracted() {

        // GIVEN
        String code = "012";
        Mockito.when(cell.getCellType()).thenReturn(CellType.STRING);
        Mockito.when(cell.getStringCellValue()).thenReturn(code);
        Mockito.doNothing().when(rowData).setMunicipalityCode(Mockito.anyInt());

        // WHEN
        reader.extractMunicipalityCode(cell, rowData);

        // THEN
        Mockito.verify(rowData).setMunicipalityCode(Integer.valueOf(code));
    }

    @Test
    void extractMunicipalityCode_cellIsBlankAndPartOfMergedRegion_valueFromMergedRegionIsExtracted() {

        // GIVEN
        Mockito.when(cell.getCellType()).thenReturn(CellType.BLANK);

        int columnIndex = 1;
        int rowIndex = 2;
        Mockito.when(cell.getColumnIndex()).thenReturn(columnIndex);
        Mockito.when(cell.getRowIndex()).thenReturn(rowIndex);
        Mockito.when(mergedRegion.containsColumn(Mockito.anyInt())).thenReturn(true);
        Mockito.when(mergedRegion.containsRow(Mockito.anyInt())).thenReturn(true);

        int firstColumn = 0;
        int firstRow = 1;
        Mockito.when(mergedRegion.getFirstColumn()).thenReturn(firstColumn);
        Mockito.when(mergedRegion.getFirstRow()).thenReturn(firstRow);

        Cell firstCell = Mockito.mock(Cell.class);
        Row row = Mockito.mock(Row.class);
        Mockito.when(sheet.getRow(Mockito.anyInt())).thenReturn(row);
        Mockito.when(row.getCell(Mockito.anyInt())).thenReturn(firstCell);

        String code = "012";
        Mockito.when(firstCell.getStringCellValue()).thenReturn(code);
        Mockito.doNothing().when(rowData).setMunicipalityCode(Mockito.anyInt());

        // WHEN
        reader.extractMunicipalityCode(cell, rowData);

        // THEN
        Mockito.verify(mergedRegion).containsColumn(columnIndex);
        Mockito.verify(mergedRegion).containsRow(rowIndex);
        Mockito.verify(sheet).getRow(firstRow);
        Mockito.verify(row).getCell(firstColumn);
        Mockito.verify(rowData).setMunicipalityCode(Integer.valueOf(code));
    }

    @Test
    void extractMunicipalityTitle_cellWithStringValueIsGiven_municipalityTitleIsExtracted() {

        // GIVEN
        String title = "title";
        Mockito.when(cell.getCellType()).thenReturn(CellType.STRING);
        Mockito.when(cell.getStringCellValue()).thenReturn(title);
        Mockito.doNothing().when(rowData).setMunicipalityTitle(Mockito.anyString());

        // WHEN
        reader.extractMunicipalityTitle(cell, rowData);

        // THEN
        Mockito.verify(rowData).setMunicipalityTitle(title);
    }

    @Test
    void extractMunicipalityTitle_cellIsBlankAndPartOfMergedRegion_valueFromMergedRegionIsExtracted() {

        // GIVEN
        Mockito.when(cell.getCellType()).thenReturn(CellType.BLANK);

        int columnIndex = 1;
        int rowIndex = 2;
        Mockito.when(cell.getColumnIndex()).thenReturn(columnIndex);
        Mockito.when(cell.getRowIndex()).thenReturn(rowIndex);
        Mockito.when(mergedRegion.containsColumn(Mockito.anyInt())).thenReturn(true);
        Mockito.when(mergedRegion.containsRow(Mockito.anyInt())).thenReturn(true);

        int firstColumn = 0;
        int firstRow = 1;
        Mockito.when(mergedRegion.getFirstColumn()).thenReturn(firstColumn);
        Mockito.when(mergedRegion.getFirstRow()).thenReturn(firstRow);

        Cell firstCell = Mockito.mock(Cell.class);
        Row row = Mockito.mock(Row.class);
        Mockito.when(sheet.getRow(Mockito.anyInt())).thenReturn(row);
        Mockito.when(row.getCell(Mockito.anyInt())).thenReturn(firstCell);

        String title = "municipality title";
        Mockito.when(firstCell.getStringCellValue()).thenReturn(title);
        Mockito.doNothing().when(rowData).setMunicipalityTitle(Mockito.anyString());

        // WHEN
        reader.extractMunicipalityTitle(cell, rowData);

        // THEN
        Mockito.verify(mergedRegion).containsColumn(columnIndex);
        Mockito.verify(mergedRegion).containsRow(rowIndex);
        Mockito.verify(sheet).getRow(firstRow);
        Mockito.verify(row).getCell(firstColumn);
        Mockito.verify(rowData).setMunicipalityTitle(title);
    }

    @Test
    void extractYear_cellWithNumericValueIsGiven_yearIsExtracted() {

        // GIVEN
        int year = 2020;
        Mockito.when(cell.getCellType()).thenReturn(CellType.NUMERIC);
        Mockito.when(cell.getNumericCellValue()).thenReturn((double) year);
        Mockito.doNothing().when(rowData).setYear(Mockito.anyInt());

        // WHEN
        reader.extractYear(cell, rowData);

        // THEN
        Mockito.verify(rowData).setYear(year);
    }

    @Test
    void extractYear_cellWithStringValueIsGiven_yearIsExtracted() {

        // GIVEN
        String year = "2020";
        Mockito.when(cell.getCellType()).thenReturn(CellType.STRING);
        Mockito.when(cell.getStringCellValue()).thenReturn(year);
        Mockito.doNothing().when(rowData).setYear(Mockito.anyInt());

        // WHEN
        reader.extractYear(cell, rowData);

        // THEN
        Mockito.verify(rowData).setYear(Integer.valueOf(year));
    }

    @Test
    void extractObjectName_cellWithStringValueIsGiven_objectNameIsExtracted() {

        // GIVEN
        String objectName = "object name";
        Mockito.when(cell.getCellType()).thenReturn(CellType.STRING);
        Mockito.when(cell.getStringCellValue()).thenReturn(objectName);
        Mockito.doNothing().when(rowData).setObjectName(Mockito.anyString());

        // WHEN
        reader.extractObjectName(cell, rowData);

        // WHEN
        Mockito.verify(rowData).setObjectName(objectName);
    }

    @Test
    void extractObjectName_cellIsBlankAndIsPartOfMergedRegion_valueIsExtractedFromMergedRegion() {

        // GIVEN
        Mockito.when(cell.getCellType()).thenReturn(CellType.BLANK);

        int columnIndex = 1;
        int rowIndex = 2;
        Mockito.when(cell.getColumnIndex()).thenReturn(columnIndex);
        Mockito.when(cell.getRowIndex()).thenReturn(rowIndex);
        Mockito.when(mergedRegion.containsColumn(Mockito.anyInt())).thenReturn(true);
        Mockito.when(mergedRegion.containsRow(Mockito.anyInt())).thenReturn(true);

        int firstColumn = 0;
        int firstRow = 1;
        Mockito.when(mergedRegion.getFirstColumn()).thenReturn(firstColumn);
        Mockito.when(mergedRegion.getFirstRow()).thenReturn(firstRow);

        Cell firstCell = Mockito.mock(Cell.class);
        Row row = Mockito.mock(Row.class);
        Mockito.when(sheet.getRow(Mockito.anyInt())).thenReturn(row);
        Mockito.when(row.getCell(Mockito.anyInt())).thenReturn(firstCell);

        String objectName = "object name";
        Mockito.when(firstCell.getStringCellValue()).thenReturn(objectName);
        Mockito.doNothing().when(rowData).setObjectName(Mockito.anyString());

        // WHEN
        reader.extractObjectName(cell, rowData);

        // THEN
        Mockito.verify(mergedRegion).containsColumn(columnIndex);
        Mockito.verify(mergedRegion).containsRow(rowIndex);
        Mockito.verify(sheet).getRow(firstRow);
        Mockito.verify(row).getCell(firstColumn);
        Mockito.verify(rowData).setObjectName(objectName);
    }

    @Test
    void extractAddress_cellWithStringValueIsGiven_addressIsExtracted() {

        // GIVEN
        String address = "address";
        Mockito.when(cell.getStringCellValue()).thenReturn(address);
        Mockito.doNothing().when(rowData).setAddress(Mockito.anyString());

        // WHEN
        reader.extractAddress(cell, rowData);

        // THEN
        Mockito.verify(rowData).setAddress(address);
    }

    @Test
    void extractCadastralNumber_cellWithCadastralNumberIsGiven_cadastralNumberIsExtracted() {

        // GIVEN
        String cadastralNumber = "cadastral number";
        Mockito.when(cell.getStringCellValue()).thenReturn(cadastralNumber);
        Mockito.doNothing().when(rowData).setCadastralNumbers(Mockito.anyString());

        // WHEN
        reader.extractCadastralNumber(cell, rowData);

        // THEN
        Mockito.verify(rowData).setCadastralNumbers(cadastralNumber);
    }
    
    @Test
    void extractCadastralNumber_cellWithoutCadastralNumberIsGiven_cadastralNumberIsNotSet() {

        // GIVEN
        String cadastralNumber = "нет";
        Mockito.when(cell.getStringCellValue()).thenReturn(cadastralNumber);

        // WHEN
        reader.extractCadastralNumber(cell, rowData);

        // THEN
        Mockito.verifyNoInteractions(rowData);
    }

    @Test
    void extractAdditionalNumber_cellWithAdditionalNumberIsGiven_additionalNumberIsExtracted() {

        // GIVEN
        String additionalNumber = "additional number";
        Mockito.when(cell.getCellType()).thenReturn(CellType.STRING);
        Mockito.when(cell.getStringCellValue()).thenReturn(additionalNumber);
        Mockito.doNothing().when(rowData).setAdditionalNumber(Mockito.anyString());

        // WHEN
        reader.extractAdditionalNumber(cell, rowData);

        // THEN
        Mockito.verify(rowData).setAdditionalNumber(additionalNumber);
    }

    @Test
    void extractAdditionalNumber_blankCellIsGiven_additionalNumberIsNotSet() {

        // GIVEN
        Mockito.when(cell.getCellType()).thenReturn(CellType.BLANK);

        // WHEN
        reader.extractAdditionalNumber(cell, rowData);

        // THEN
        Mockito.verifyNoInteractions(rowData);
    }

    @Test
    void extractAdditionalNumber_cellWithoutAdditionalNumberIsGiven_additionalNumberIsNotSet() {

        // GIVEN
        String additionalNumber = "нет";
        Mockito.when(cell.getCellType()).thenReturn(CellType.STRING);
        Mockito.when(cell.getStringCellValue()).thenReturn(additionalNumber);

        // WHEN
        reader.extractAdditionalNumber(cell, rowData);

        // THEN
        Mockito.verifyNoInteractions(rowData);
    }

    @Test
    void extractArea_cellWithNumericValueIsGiven_areaIsExtracted() {

        // GIVEN
        double area = 123.4;
        Mockito.when(cell.getCellType()).thenReturn(CellType.NUMERIC);
        Mockito.when(cell.getNumericCellValue()).thenReturn(area);
        Mockito.doNothing().when(rowData).setArea(Mockito.anyDouble());

        // WHEN
        reader.extractArea(cell, rowData);

        // THEN
        Mockito.verify(rowData).setArea(area);
    }

    @Test
    void extractArea_cellWithStringValueIsGiven_areaIsExtracted() {

        // GIVEN
        String area = "123,4";
        Mockito.when(cell.getCellType()).thenReturn(CellType.STRING);
        Mockito.when(cell.getStringCellValue()).thenReturn(area);
        Mockito.doNothing().when(rowData).setArea(Mockito.anyDouble());

        // WHEN
        reader.extractArea(cell, rowData);

        // THEN
        Mockito.verify(rowData).setArea(123.4);
    }

    @Test
    void extractDeclaredCost_cellWithNumericValueIsGiven_declaredCostIsExtracted() {

        // GIVEN
        double cost = 345.6;
        Mockito.when(cell.getCellType()).thenReturn(CellType.NUMERIC);
        Mockito.when(cell.getNumericCellValue()).thenReturn(cost);
        Mockito.doNothing().when(rowData).setDeclaredCost(Mockito.anyDouble());

        // WHEN
        reader.extractDeclaredCost(cell, rowData);

        // THEN
        Mockito.verify(rowData).setDeclaredCost(cost);
    }

    @Test
    void extractDeclaredCost_cellWithFormulaTypeIsGiven_valueFromFormulaIsCalculated() {

        // GIVEN
        Mockito.when(cell.getCellType()).thenReturn(CellType.FORMULA);

        Double cost = 123.4;
        CellValue cellValue = Mockito.mock(CellValue.class);
        Mockito.when(cellValue.getNumberValue()).thenReturn(cost);
        Mockito.when(formulaEvaluator.evaluate(Mockito.any(Cell.class))).thenReturn(cellValue);
        Mockito.doNothing().when(rowData).setDeclaredCost(Mockito.anyDouble());

        // WHEN
        reader.extractDeclaredCost(cell, rowData);

        // THEN
        Mockito.verify(formulaEvaluator).evaluate(cell);
        Mockito.verify(rowData).setDeclaredCost(cost);
    }

    @Test
    void extractIncludedCost_cellWithNumericValueIsGiven_includedCostIsExtracted() {

        // GIVEN
        double cost = 567.8;
        Mockito.when(cell.getCellType()).thenReturn(CellType.NUMERIC);
        Mockito.when(cell.getNumericCellValue()).thenReturn(cost);
        Mockito.doNothing().when(rowData).setIncludedCost(cost);

        // WHEN
        reader.extractIncludedCost(cell, rowData);

        // THEN
        Mockito.verify(rowData).setIncludedCost(cost);
    }

    @Test
    void extractIncludedCost_cellWithFormulaValueIsGiven_valueFromFormulaIsCalculated() {

        // GIVEN
        Mockito.when(cell.getCellType()).thenReturn(CellType.FORMULA);

        Double cost = 123.4;
        CellValue cellValue = Mockito.mock(CellValue.class);
        Mockito.when(cellValue.getNumberValue()).thenReturn(cost);
        Mockito.when(formulaEvaluator.evaluate(Mockito.any(Cell.class))).thenReturn(cellValue);
        Mockito.doNothing().when(rowData).setIncludedCost(Mockito.anyDouble());

        // WHEN
        reader.extractIncludedCost(cell, rowData);

        // THEN
        Mockito.verify(formulaEvaluator).evaluate(cell);
        Mockito.verify(rowData).setIncludedCost(cost);
    }

    @Test
    void extractIncludedCost_cellIsBlankAndIsPartOfMergedRegion_includedCostIsNotSet() {

        // GIVEN
        Mockito.when(cell.getCellType()).thenReturn(CellType.BLANK);

        int columnIndex = 1;
        int rowIndex = 2;
        Mockito.when(cell.getColumnIndex()).thenReturn(columnIndex);
        Mockito.when(cell.getRowIndex()).thenReturn(rowIndex);
        Mockito.when(mergedRegion.containsColumn(Mockito.anyInt())).thenReturn(true);
        Mockito.when(mergedRegion.containsRow(Mockito.anyInt())).thenReturn(true);

        // WHEN
        reader.extractIncludedCost(cell, rowData);

        // THEN
        Mockito.verify(mergedRegion).containsColumn(columnIndex);
        Mockito.verify(mergedRegion).containsRow(rowIndex);
        Mockito.verifyNoInteractions(rowData);
    }

    @Test
    void extractIncludedCost_cellIsBlankAndIsNotPartOfMergedRegion_includedCostIsSetToZero() {

        // GIVEN
        Mockito.when(cell.getCellType()).thenReturn(CellType.BLANK);

        int columnIndex = 1;
        Mockito.when(cell.getColumnIndex()).thenReturn(columnIndex);
        Mockito.when(mergedRegion.containsColumn(Mockito.anyInt())).thenReturn(false);
        Mockito.doNothing().when(rowData).setIncludedCost(Mockito.anyDouble());

        // WHEN
        reader.extractIncludedCost(cell, rowData);

        // THEN
        Mockito.verify(mergedRegion).containsColumn(columnIndex);
        Mockito.verify(rowData).setIncludedCost(0.0);
    }

    @Test
    void extractRejectionReason_cellWithStringValueIsGiven_rejectionReasonIsExtracted() {

        // GIVEN
        String reason = "reject";
        Mockito.when(cell.getCellType()).thenReturn(CellType.STRING);
        Mockito.when(cell.getStringCellValue()).thenReturn(reason);
        Mockito.doNothing().when(rowData).setRejectionReason(Mockito.anyString());

        // WHEN
        reader.extractRejectionReason(cell, rowData);

        // THEN
        Mockito.verify(rowData).setRejectionReason(reason);
    }

    @Test
    void extractRejectionReason_blankCellIsGiven_rejectionReasonIsNotSet() {

        // GIVEN
        Mockito.when(cell.getCellType()).thenReturn(CellType.BLANK);

        // WHEN
        reader.extractRejectionReason(cell, rowData);

        // THEN
        Mockito.verifyNoInteractions(rowData);
    }

    @Test
    void extractRepeated_cellIsNotBlank_isRepeatedSetTrue() {

        // GIVEN
        Mockito.when(cell.getCellType()).thenReturn(CellType.STRING);
        Mockito.doNothing().when(rowData).setRepeated(Mockito.anyBoolean());

        // WHEN
        reader.extractRepeated(cell, rowData);

        // THEN
        Mockito.verify(rowData).setRepeated(true);
    }

    @Test
    void extractRepeated_cellIsBlank_isRepeatedSetFalse() {

        // GIVEN
        Mockito.when(cell.getCellType()).thenReturn(CellType.BLANK);
        Mockito.doNothing().when(rowData).setRepeated(Mockito.anyBoolean());

        // WHEN
        reader.extractRepeated(cell, rowData);

        // THEN
        Mockito.verify(rowData).setRepeated(false);
    }

    @Test
    void extractExecutionCost_cellWithNumericValueIsGiven_executionCostIsExtracted() {

        // GIVEN
        int columnIndex = 1;
        Mockito.when(cell.getColumnIndex()).thenReturn(columnIndex);
        Mockito.when(columns.get(Mockito.anyInt())).thenReturn(ColumnNames.FIRST_QUARTER);

        double cost = 11.22;
        Mockito.when(cell.getCellType()).thenReturn(CellType.NUMERIC);
        Mockito.when(cell.getNumericCellValue()).thenReturn(cost);

        Map<ExecutionPeriod, Double> costs = new HashMap<>();
        Mockito.when(rowData.getExecutionCosts()).thenReturn(costs);

        // WHEN
        reader.extractExecutionCost(cell, rowData);

        // THEN
        Mockito.verify(columns).get(columnIndex);
        Assertions.assertThat(costs).containsAllEntriesOf(Map.of(ExecutionPeriod.FIRST_QUARTER, cost));
    }
}

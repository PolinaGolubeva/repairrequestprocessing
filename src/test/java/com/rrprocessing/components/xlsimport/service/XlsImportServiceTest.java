package com.rrprocessing.components.xlsimport.service;

import static org.assertj.core.api.Assertions.assertThat;

import com.rrprocessing.components.cadastralnumber.domain.CadastralNumber;
import com.rrprocessing.components.cadastralnumber.service.CadastralNumberService;
import com.rrprocessing.components.municipality.domain.Municipality;
import com.rrprocessing.components.municipality.service.MunicipalityService;
import com.rrprocessing.components.request.dao.RequestDao;
import com.rrprocessing.components.request.domain.RepairRequest;
import com.rrprocessing.components.requeststatus.domain.RequestStatus;
import com.rrprocessing.components.requeststatus.service.RequestStatusService;
import com.rrprocessing.components.xlsimport.converter.RowDataToRepairRequestConverter;
import com.rrprocessing.components.xlsimport.dto.RowData;
import com.rrprocessing.components.xlsimport.reader.XlsReader;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class XlsImportServiceTest {

    @Spy
    @InjectMocks
    private XlsImportServiceImpl importService;

    @Mock
    private XlsReader reader;

    @Mock
    private RowDataToRepairRequestConverter converter;

    @Mock
    private RequestDao requestDao;

    @Mock
    private RequestStatusService requestStatusService;

    @Mock
    private MunicipalityService municipalityService;

    @Mock
    private CadastralNumberService cadastralNumberService;

    @Mock
    private RowData rowData1;

    @Mock
    private RowData rowData2;

    @Mock
    private RepairRequest request1;

    @Mock
    private RepairRequest request2;

    private static final String FILENAME = "some_file.xlsx";

    @Test
    void importData_readerReturnedDataFromFile_dataIsSaved() {

        // GIVEN
        Mockito.when(reader.extractData(Mockito.anyString())).thenReturn(List.of(rowData1, rowData2));
        Mockito.when(importService.convertToRequests(Mockito.anyList())).thenReturn(List.of(request1, request2));
        Mockito.when(importService.filterExistingRepairRequests(Mockito.anyList())).thenReturn(List.of(request1, request2));

        Mockito.doNothing().when(importService).saveMunicipalities(Mockito.anyList());
        Mockito.doNothing().when(importService).saveCadastralNumbers(Mockito.anyList());
        Mockito.doNothing().when(importService).saveRequests(Mockito.anyList());

        // WHEN
        importService.importData(FILENAME);

        // THEN
        Mockito.verify(reader).extractData(FILENAME);
        Mockito.verify(importService).convertToRequests(List.of(rowData1, rowData2));
        Mockito.verify(importService).filterExistingRepairRequests(List.of(request1, request2));
        Mockito.verify(importService).saveMunicipalities(List.of(request1, request2));
        Mockito.verify(importService).saveCadastralNumbers(List.of(request1, request2));
        Mockito.verify(importService).saveRequests(List.of(request1, request2));
    }

    @Test
    void convertToRequests_rowDataListIsGiven_requestsAreReturned() {

        // GIVEN
        Mockito.when(converter.convert(rowData1)).thenReturn(request1);
        Mockito.when(converter.convert(rowData2)).thenReturn(request2);

        // WHEN
        List<RepairRequest> actual = importService.convertToRequests(List.of(rowData1, rowData2));

        // THEN
        assertThat(actual).containsExactly(request1, request2);
    }

    @Test
    void filterExistingRepairRequests_newAndExistingRequestsArePresent_newRequestsAreReturned() {

        // GIVEN
        int year = 2020;
        int code1 = 123;
        int code2 = 345;
        Municipality municipality1 = new Municipality();
        municipality1.setCode(code1);
        Municipality municipality2 = new Municipality();
        municipality2.setCode(code2);

        RepairRequest newRequest1 = RepairRequest.builder()
                .municipality(municipality1)
                .year(year)
                .objectName("object1")
                .address("address1")
                .additionalNumber("additional1")
                .build();
        RepairRequest newRequest2 = RepairRequest.builder()
                .municipality(municipality2)
                .year(year)
                .objectName("object2")
                .address("address2")
                .additionalNumber("additional2")
                .build();

        RepairRequest oldRequest1 = RepairRequest.builder()
                .municipality(municipality1)
                .year(year)
                .objectName("object1")
                .address("address3")
                .additionalNumber("additional1")
                .build();
        RepairRequest oldRequest2 = RepairRequest.builder()
                .municipality(municipality2)
                .year(year)
                .objectName("object4")
                .address("address2")
                .additionalNumber("additional2")
                .build();
        RepairRequest existingRequest = RepairRequest.builder()
                .municipality(municipality1)
                .year(year)
                .objectName("object5")
                .address("address5")
                .build();

        Mockito.when(requestDao.findByYearAndMunicipality(year, code1)).thenReturn(List.of(oldRequest1, existingRequest));
        Mockito.when(requestDao.findByYearAndMunicipality(year, code2)).thenReturn(List.of(oldRequest2));

        // WHEN
        List<RepairRequest> actual = importService.filterExistingRepairRequests(List.of(newRequest1, newRequest2, oldRequest1, oldRequest2));

        // THEN
        assertThat(actual).containsExactly(newRequest1, newRequest2);
    }

    @Test
    void saveMunicipalities_requestsAreGiven_municipalitiesAreSaved() {

        // GIVEN
        Municipality municipality1 = Mockito.mock(Municipality.class);
        Municipality municipality2 = Mockito.mock(Municipality.class);
        Mockito.when(request1.getMunicipality()).thenReturn(municipality1);
        Mockito.when(request2.getMunicipality()).thenReturn(municipality2);
        Mockito.when(municipalityService.save(Mockito.anyList())).thenReturn(List.of(municipality1, municipality2));

        int code1 = 123;
        Mockito.when(municipality1.getCode()).thenReturn(code1);
        int code2 = 345;
        Mockito.when(municipality2.getCode()).thenReturn(code2);

        Mockito.doNothing().when(request1).setMunicipality(Mockito.any(Municipality.class));
        Mockito.doNothing().when(request2).setMunicipality(Mockito.any(Municipality.class));

        // WHEN
        importService.saveMunicipalities(List.of(request1, request2));

        // THEN
        Mockito.verify(municipalityService).save(List.of(municipality1, municipality2));
        Mockito.verify(request1).setMunicipality(municipality1);
        Mockito.verify(request2).setMunicipality(municipality2);
    }

    @Test
    void saveCadastralNumbers_requestsAreGiven_cadastralNumbersAreSaved() {

        // GIVEN
        String number1 = "cadastral1";
        String number2 = "cadastral2";
        String number3 = "cadastral3";
        CadastralNumber cadastralNumber1 = new CadastralNumber();
        cadastralNumber1.setNumber(number1);
        CadastralNumber cadastralNumber2 = new CadastralNumber();
        cadastralNumber2.setNumber(number2);
        CadastralNumber cadastralNumber3 = new CadastralNumber();
        cadastralNumber3.setNumber(number3);

        Mockito.when(request1.getCadastralNumbers()).thenReturn(List.of(cadastralNumber1, cadastralNumber2));
        Mockito.when(request2.getCadastralNumbers()).thenReturn(List.of(cadastralNumber2, cadastralNumber3));
        Mockito.when(cadastralNumberService.save(Mockito.anyList())).thenReturn(List.of(cadastralNumber1, cadastralNumber2, cadastralNumber3));
        Mockito.doNothing().when(request1).setCadastralNumbers(Mockito.anyList());
        Mockito.doNothing().when(request2).setCadastralNumbers(Mockito.anyList());

        // WHEN
        importService.saveCadastralNumbers(List.of(request1, request2));

        // THEN
        Mockito.verify(cadastralNumberService).save(List.of(cadastralNumber1, cadastralNumber2, cadastralNumber3));
        Mockito.verify(request1).setCadastralNumbers(List.of(cadastralNumber1, cadastralNumber2));
        Mockito.verify(request2).setCadastralNumbers(List.of(cadastralNumber2, cadastralNumber3));
    }

    @Test
    void saveRequests_requestsAreGiven_requestsAreSaved() {

        // GIVEN
        RepairRequest request3 = Mockito.mock(RepairRequest.class);

        RequestStatus requestStatus1 = Mockito.mock(RequestStatus.class);
        RequestStatus requestStatus2 = Mockito.mock(RequestStatus.class);
        Mockito.when(request1.getRequestStatus()).thenReturn(requestStatus1);
        Mockito.when(request2.getRequestStatus()).thenReturn(requestStatus2);
        Mockito.when(requestStatusService.save(Mockito.any(RequestStatus.class))).then(AdditionalAnswers.returnsFirstArg());

        Mockito.doNothing().when(request1).setRequestStatus(Mockito.any(RequestStatus.class));
        Mockito.doNothing().when(request2).setRequestStatus(Mockito.any(RequestStatus.class));
        Mockito.doNothing().when(request3).setRequestStatus(Mockito.any(RequestStatus.class));

        // WHEN
        importService.saveRequests(List.of(request1, request2, request3));

        // THEN
        Mockito.verify(requestStatusService).save(requestStatus1);
        Mockito.verify(request1).setRequestStatus(requestStatus1);
        Mockito.verify(requestDao).save(request1);

        Mockito.verify(requestStatusService).save(requestStatus2);
        Mockito.verify(request2).setRequestStatus(requestStatus2);
        Mockito.verify(requestDao).save(request2);

        Mockito.verifyNoMoreInteractions(requestStatusService);
        Mockito.verify(request3).setRequestStatus(requestStatus2);
        Mockito.verify(requestDao).save(request3);
    }
}

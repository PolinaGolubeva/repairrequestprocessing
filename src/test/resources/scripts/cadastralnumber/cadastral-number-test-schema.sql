CREATE TABLE IF NOT EXISTS cadastral_number (
    id SERIAL PRIMARY KEY,
    cad_number VARCHAR UNIQUE
);
CREATE TABLE IF NOT EXISTS municipality (
    id SERIAL PRIMARY KEY,
    code INTEGER UNIQUE,
    title VARCHAR UNIQUE
);

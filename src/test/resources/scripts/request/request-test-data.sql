INSERT INTO municipality (id, code, title) VALUES (11, 111, 'municipality 111'),
                                                  (12, 222, 'municipality 222');

INSERT INTO work_type(id, title) VALUES (21, 'work type');

INSERT INTO additional_number(id, add_number) VALUES (31, 'additional number'),
                                                     (32, 'another additional number');

INSERT INTO cadastral_number(id, cad_number) VALUES (41, 'cadastral number 1'),
                                                    (42, 'cadastral number 2');

INSERT INTO request (id, municipality_id, request_year, object_name, address, work_type_id, additional_number_id,
                     area, declared_cost, request_status_id, is_request_repeated)
    VALUES (51, 11, 2017, 'object 1', 'address 1', 21, 31, 111.00, 1111.00, 1, false),
           (52, 12, 2017, 'object 2', 'address 2', 21, NULL, 222.00, 2222.00, 2, true),
           (53, 12, 2017, 'object 3', 'address 3', 21, 32, 333.00, 3333.00, 3, true),
           (54, 12, 2018, 'object 4', 'address 4', 21, NULL, 444.00, 4444.00, 4, false);

INSERT INTO request_cadastral_number (id, request_id, cadastral_id) VALUES (61, 51, 41), (62, 52, 41),
                                                                           (63, 52, 42), (64, 53, 42);
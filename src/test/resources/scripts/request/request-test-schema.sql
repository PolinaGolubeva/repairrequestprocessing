CREATE TABLE IF NOT EXISTS municipality (
    id SERIAL PRIMARY KEY,
    code INTEGER,
    title VARCHAR
);

CREATE TABLE IF NOT EXISTS work_type (
    id SERIAL PRIMARY KEY,
    title VARCHAR
);

CREATE TABLE IF NOT EXISTS additional_number (
    id SERIAL PRIMARY KEY,
    add_number VARCHAR
);

CREATE TABLE IF NOT EXISTS request (
    id SERIAL PRIMARY KEY,
    municipality_id INTEGER,
    request_year INTEGER,
    object_name VARCHAR,
    address VARCHAR,
    work_type_id INTEGER,
    additional_number_id INTEGER,
    area NUMERIC(10, 2),
    declared_cost NUMERIC (10,2),
    request_status_id INTEGER,
    is_request_repeated BOOLEAN,

    CONSTRAINT fk_request_municipality
        FOREIGN KEY (municipality_id)
        REFERENCES municipality(id),
    CONSTRAINT fk_request_work_type
        FOREIGN KEY (work_type_id)
        REFERENCES work_type(id),
    CONSTRAINT fk_request_additional_number
        FOREIGN KEY (additional_number_id)
        REFERENCES additional_number(id)
);

CREATE TABLE IF NOT EXISTS cadastral_number (
    id SERIAL PRIMARY KEY,
    cad_number VARCHAR UNIQUE
);

CREATE TABLE IF NOT EXISTS request_cadastral_number (
    id SERIAL PRIMARY KEY,
    request_id INTEGER,
    cadastral_id INTEGER,

    CONSTRAINT fk_request_cadastral_number_request
        FOREIGN KEY (request_id)
        REFERENCES request(id),
    CONSTRAINT fk_request_cadastral_number_cadastral_number
        FOREIGN KEY (cadastral_id)
        REFERENCES cadastral_number(id)
);

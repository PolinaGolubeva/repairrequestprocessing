INSERT INTO execution_period (id, title) VALUES (1, '1 кв'), (2, '2 кв'), (3, '3 кв'), (4, '4 кв');

INSERT INTO comment (id, text) VALUES (11, 'some comment'), (12, 'other comment');

INSERT INTO request_status (id, is_rejected, comment_id, included_cost)
VALUES (21, true, 11, 0.0), (22, true, NULL, 0.0),
       (23, false, NULL, 333.33), (24, false, 12, 444.44);

INSERT INTO request_execution (id, execution_period_id, cost, request_status_id)
VALUES (31, 1, 11.11, 23), (32, 2, 22.22, 23), (33, 3, 33.33, 23), (34, 4, 44.44, 24);

INSERT INTO request (id, request_status_id) VALUES (41, 21), (42, 22), (43, 23), (44, 24);
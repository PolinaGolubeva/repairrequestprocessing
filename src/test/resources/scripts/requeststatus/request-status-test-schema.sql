CREATE TABLE IF NOT EXISTS comment (
    id SERIAL PRIMARY KEY,
    text VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS execution_period (
    id SERIAL PRIMARY KEY,
    title VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS request_status (
    id SERIAL PRIMARY KEY,
    is_rejected BOOLEAN,
    comment_id INTEGER,
    included_cost NUMERIC(10,2),

    CONSTRAINT fk_request_status_comment
        FOREIGN KEY (comment_id)
        REFERENCES comment(id)
);

CREATE TABLE IF NOT EXISTS request_execution (
    id SERIAL PRIMARY KEY,
    execution_period_id INTEGER,
    cost NUMERIC(10,2),
    request_status_id INTEGER,

    CONSTRAINT fk_request_execution_execution_period
        FOREIGN KEY (execution_period_id)
        REFERENCES execution_period(id),
    CONSTRAINT fk_request_execution_request_status
        FOREIGN KEY (request_status_id)
        REFERENCES request_status
);

CREATE TABLE IF NOT EXISTS request (
    id SERIAL PRIMARY KEY,
    request_status_id INTEGER,

    CONSTRAINT fk_request_request_status
        FOREIGN KEY (request_status_id)
        REFERENCES request_status(id)
);
